#ifndef VBFllvvHM_H
#define VBFllvvHM_H
#include "NtupleMaker.h"
#include "tools.h"
#include <math.h>
#if not defined(__CINT__) || defined(__MAKECINT__)
#include "TMVA/Tools.h"
#include "TMVA/Reader.h"
#include "TMVA/MethodCuts.h"
#endif


    using namespace TMVA;

int FindBinSTXS(double Zpt);
int FindBinEWK(double Zpt);

class VBFllvvHM : public NtupleMaker{
public:
//	using NtupleMaker::NtupleMaker;
	VBFllvvHM(string Input,int SampleType);
	~VBFllvvHM(){};
	double FileNormSF;
	bool doVBFSel;
	bool doShapeSYS;
        int  doAddCorr;
	void ReadTreeSkimed(TTree* sTree);
	void LoopEMuTau(TTree* sTree, string inName);
	void LoopEMuTau(TTree* sTree, TFile *InputFile,string inName);
	void LoopEMuTau(TTree* sTree, TFile *InputFile,string inName,TFile* ADSF);
	void Loop3lCR(TTree* sTree, string inName);
	void RunProcessor();
	void RunProcessor(string treename);
	void RunProcessor3lCR();
	double GetNormSF(int run, string FileName,string OutFileName);
	double GetNormSF(int run, TFile *InputFile,string FileName );
	void NormPlot( MakeHist* InClass, double NSF);
	void AddPlot(MakeHist*Base,MakeHist*Indep);
	void WritePlot(MakeHist*Base, TFile*OutFile);
	void WriteCutFlow(MakeHist*Base, TFile*OutFile);
	TTree *skimTree;
	TFile *skimFile;
        string SYSType;
//TH1D* AddSF;

   Float_t a_zhvvbbcorr[95] = {
        0.0146846,  0.0136521,   0.0125801,   0.0117771,   0.010976,
        0.00989665, 0.00929942,  0.00836484,  0.00781992,  0.00733247,
        0.00688885, 0.00666833,  0.0063354,   0.00637412,  0.00662595,
        0.0069015,  0.00716689,  0.00760953,  0.00823267,  0.00914484,
        0.00960494, 0.0110894,   0.0122241,   0.0127155,   0.0126892,
        0.0125873,  0.01278,     0.0128243,   0.0118519,   0.0116125,
        0.0102697,  0.00960959,  0.00929141,  0.00807739,  0.00588976,
        0.00522135, 0.00365527,  0.00214147,  0.000569382, 0.000322672,
        -0.0015679, -0.00345846, -0.00534903, -0.0072396,  -0.00913017,
        -0.0110207, -0.0129113,  -0.0148019,  -0.0166924,  -0.018583,
        -0.0204736, -0.0223641,  -0.0242547,  -0.0261453,  -0.0280358,
        -0.0299264, -0.031817,   -0.0337076,  -0.0355981,  -0.0374887,
        -0.0393793, -0.0412698,  -0.0431604,  -0.045051,   -0.0469415,
        -0.0488321, -0.0507227,  -0.0526132,  -0.0545038,  -0.0563944,
        -0.0582849, -0.0601755,  -0.0620661,  -0.0639566,  -0.0658472,
        -0.0677378, -0.0696283,  -0.0715189,  -0.0734095,  -0.0753001,
        -0.0771906, -0.0790812,  -0.0809718,  -0.0828623,  -0.0847529,
        -0.0866435, -0.088534,   -0.0904246,  -0.0923152,  -0.0942057,
        -0.0960963, -0.0979869,  -0.0998774,  -0.101768,   -0.103659};

    Float_t a_zhllbbcorr[95] = {
        0.0180518,  0.0175002,  0.0132077,  0.0116164,  0.0122393,  0.00626285,
        0.00409211, 0.00514318, 0.0013087,  0.0006585,  -0.0028916, -0.0024331,
        -0.0050633, -0.0070058, -0.0024234, -0.0077263, -0.0099203, -0.0079423,
        -0.0024083, -0.0079295, -0.0140968, -0.0084094, -0.0091692, 0.0452771,
        -0.0587156, -0.0069676, -0.0047452, -0.0114475, -0.0114,    -0.0165374,
        -0.0013505, -0.0192127, -0.0103618, -0.0198998, -0.0228517, -0.0141691,
        -0.0247783, -0.0313323, -0.0254595, -0.0212254, -0.0351162, -0.033811,
        -0.0392759, -0.0285445, -0.0411198, -0.0374336, -0.0600432, -0.0387952,
        -0.0455118, -0.0681619, -0.0451166, -0.0476096, -0.0491737, -0.0710518,
        -0.0556638, -0.088824,  -0.0299995, -0.0827677, -0.0545532, -0.0744806,
        -0.0811843, -0.08833,   -0.064955,  -0.090745,  -0.0534193, -0.113034,
        -0.0849838, -0.114451,  -0.0691412, -0.099921,  -0.140833,  -0.0594616,
        -0.111809,  -0.11322,   -0.092648,  -0.121806,  -0.108687,  -0.095612,
        -0.161716,  -0.0803666, -0.140912,  -0.090183,  -0.160523,  -0.0537544,
        -0.21176,   -0.141927,  -0.105494,  -0.111952,  -0.167416,  -0.143798,
        -0.138735,  -0.143633,  -0.149896,  -0.111595,  -0.163634};



        double qqC1[5]={0.007,0.007,0.007,0.007,0.007};
        double qqC2[5]={-0.029,0.033,0.033,0.033,0.033};
        double qqC3[5]={0,-0.0053,0.013,0.013,0.013};
        double qqC4[5]={-0,0.0,-0.0041,0.014,0.014};
        double qqC5[5]={-0.00,0.0,0.0,-0.0038,0.018};
        double qqC6J0[5]={-0.03,-0.036,-0.041,-0.054,-0.068};
        double qqC6J1[5]={0.068,0.06,0.051,0.053,0.055};
        double qqC6J2[5]={0.068,0.06,0.051,0.053,0.055};
        double qqC7J1[5]={-0.045,-0.047,-0.05,-0.05,-0.057};
        double qqC7J2[5]={-0.1,0.093,0.08,0.067,0.067};

        double ggC1[5] = {0.25,0.25,0.25,0.25,0.25};
        double ggC2[5] = {-1,0.26,0.26,0.26,0.26};
        double ggC3[5] = {0,-0.094,0.13,0.13,0.13};
        double ggC4[5] = {0,-0.0,-0.026,0.14,0.14};
        double ggC5[5] = {0,-0.0,0.0,-0.013,0.15};
        double ggC6J0[5] = {-0.31,-0.3,-0.5,-1,-1};
        double ggC6J1[5] = {0.25,0.25,0.26,0.28,0.3};
        double ggC6J2[5] = {0.25,0.25,0.26,0.28,0.3};
        double ggC7J1[5] = {-0.15,-0.15,-0.2,-0.38,-0.66};
        double ggC7J2[5] = {0.25,0.26,0.26,0.28,0.3};


	MakeHist* VBFPlot;
	MakeHist* VBFPlotee;
	MakeHist* VBFPlotmumu;
	MakeHist* VBFPlotemu;
	MakeHist* ETauPlot;
	MakeHist* EMuPlot;
	MakeHist* MuTauPlot;
//	vector<string> InputSkimFileList;
	MakeHist* ETauPlotTotal;
	MakeHist* EMuPlotTotal;
	MakeHist* MuTauPlotTotal;
	MakeHist* VBFPlotTotal;
	MakeHist* VBFPloteeTotal;
	MakeHist* VBFPlotmumuTotal;
	MakeHist* VBFPlotemuTotal;
	MakeHist* VBFPlotAllChTotal;

	MakeHist* VBF3lCReee;
	MakeHist* VBF3lCReemu;
	MakeHist* VBF3lCRmumue;
	MakeHist* VBF3lCRmumumu;

	MakeHist* VBF3lCReeeTotal;
	MakeHist* VBF3lCReemuTotal;
	MakeHist* VBF3lCRmumueTotal;
	MakeHist* VBF3lCRmumumuTotal;
	MakeHist* VBF3lCRAllChTotal;

        map<string,MakeHist*> MCMH3lCRTotal = {
		{"mumumu",VBF3lCRmumumuTotal},
		{"mumue",VBF3lCRmumueTotal},
		{"eee",VBF3lCReeeTotal},
		{"eemu",VBF3lCReemuTotal},
		{"AllCh",VBF3lCRAllChTotal}
	};

        map<string,MakeHist*> MCMH3lCR = {
		{"mumumu",VBF3lCRmumumu},
		{"mumue",VBF3lCRmumue},
		{"eee",VBF3lCReee},
		{"eemu",VBF3lCReemu}
	};


	map<string,MakeHist*> VBFPTotal = {
		{"mumu",VBFPlotmumuTotal},
		{"ee",VBFPloteeTotal},
//		{"emu",VBFPlotemuTotal},
		{"AllCh",VBFPlotAllChTotal}
	};

	map<string,MakeHist*> VBFP= {
		{"mumu",VBFPlotmumu},
		{"ee",VBFPlotee},
//		{"emu",VBFPlotemu}
	};

	int year;
	double ChannelNumber;
	double lum;

   // Declaration of leaf types
   Int_t           run;
   Int_t           mcRandomRun;
   Int_t           bcid;
   ULong64_t       event;
   ULong64_t       PRWHash;
   Int_t           isMC;
   Int_t           mu;
   Int_t           npv;
   Int_t           event_type;
   Int_t           event_3CR;
   Int_t           SR_HM_LM;
   Float_t         weight_pileup;
   Float_t         weight_gen;
   Float_t         weight_exp;
   Float_t         weight_trig;
   Float_t         weight_jets;
   Float_t         weight_jvt;
   Float_t         weight;
   Float_t         lepplus_pt;
   Float_t         lepminus_pt;
   Float_t         lepplus_eta;
   Float_t         lepminus_eta;
   Float_t         lepplus_phi;
   Float_t         lepminus_phi;
   Float_t         lepplus_m;
   Float_t         lepminus_m;
   Int_t           medium_3rd;
   Int_t           charge_3rd;
   Float_t         lep3rd_pt;
   Float_t         lep3rd_eta;
   Float_t         lep3rd_phi;
   Float_t         lep3rd_m;
   Float_t         leading_pT_lepton;
   Float_t         subleading_pT_lepton;
   Float_t         electron_pt;
   Float_t         muon_pt;
   Float_t         electron_eta;
   Float_t         muon_eta;
   Int_t           n_jets;
   Int_t           n_cjets;
   Int_t           n_fjets;
   Float_t         pTjet_flead;
   Int_t           n_fjvtjets;
   Int_t           n_bjets;
   Float_t         leading_jet_pt;
   Float_t         leading_jet_eta;
   Float_t         leading_jet_rapidity;
   Float_t         second_jet_pt;
   Float_t         second_jet_eta;
   Float_t         second_jet_rapidity;
   Float_t         leading_twojets_vsum_pt;
   Float_t         jet_sumpt;
   Float_t         jet_vsum_pt;
   Float_t         jet_vsum_eta;
   Float_t         jet_vsum_phi;
   Float_t         jet_vsum_m;
   Float_t         mjj;
   Float_t         detajj;
   Float_t         max_mjj;
   Float_t         max_detajj;
   Float_t         cos_Z_2jets;
   Float_t         central_Z_2jets;
   Float_t         jet_central_ST;
   Float_t         jet30_central_ST;
   Float_t         jet40_central_ST;
   Float_t         jet50_central_ST;
   Int_t           n_central_jets;
   Int_t           n_central_jets30;
   Int_t           n_central_jets40;
   Int_t           n_central_jets50;
   Float_t         m_leading_jet_pt;
   Float_t         m_leading_jet_eta;
   Float_t         m_leading_jet_rapidity;
   Float_t         m_second_jet_pt;
   Float_t         m_second_jet_eta;
   Float_t         m_second_jet_rapidity;
   Int_t           m_n_central_jets;
   Float_t         m_leading_twojets_vsum_pt;
   Float_t         dPhiMET_MLeadJet;
   Float_t         dPhiMET_MSecJet;
   vector<float>   *jets_pt;
   vector<float>   *jets_eta;
   vector<float>   *jets_phi;
   vector<float>   *jets_m;
   Float_t         met_tst;
   Float_t         met_px_tst;
   Float_t         met_py_tst;
   Float_t         met_tst_loose;
   Float_t         met_px_tst_loose;
   Float_t         met_py_tst_loose;
   Float_t         met_soft;
   Float_t         met_px_soft;
   Float_t         met_py_soft;
   Float_t         met_jets_pt;
   Float_t         met_muon_pt;
   Float_t         met_electron_pt;
   Float_t         dphi_met_jets;
   Float_t         met_signi;
   Float_t         met_signif;
   Float_t         mT_ZZ;
   Float_t         Z_eta;
   Float_t         Z_pT;
   Float_t         Z_rapidity;
   Float_t         met_obj;
   Float_t         mZZ_Truth;
   Float_t         met_Truth;
   Float_t         rho_Truth;
   Float_t         ZpTomT;
   Float_t         M2Lep;
   Float_t         dLepR;
   Float_t         dMetZPhi;
   Float_t         frac_pT;
   Float_t         dPhiJ25met;
   Float_t         dPhiJ100met;
   Float_t         mT_Hinv;
   Float_t         ZpTomT_Hinv;
   Float_t         pT_Z_Truth;
   Float_t         MetOHT;
   Float_t         sumpT_scalar;
   Int_t           llvv_is_WW_ZZ;
   Float_t         weight_EL_EFF_ID_CorrUncertaintyNP0__1down;                         
   Float_t         weight_EL_EFF_ID_CorrUncertaintyNP0__1up;
   Float_t         weight_EL_EFF_ID_CorrUncertaintyNP10__1down;
   Float_t         weight_EL_EFF_ID_CorrUncertaintyNP10__1up;
   Float_t         weight_EL_EFF_ID_CorrUncertaintyNP11__1down;
   Float_t         weight_EL_EFF_ID_CorrUncertaintyNP11__1up;
   Float_t         weight_EL_EFF_ID_CorrUncertaintyNP12__1down;
   Float_t         weight_EL_EFF_ID_CorrUncertaintyNP12__1up;
   Float_t         weight_EL_EFF_ID_CorrUncertaintyNP13__1down;
   Float_t         weight_EL_EFF_ID_CorrUncertaintyNP13__1up;
   Float_t         weight_EL_EFF_ID_CorrUncertaintyNP14__1down;
   Float_t         weight_EL_EFF_ID_CorrUncertaintyNP14__1up;
   Float_t         weight_EL_EFF_ID_CorrUncertaintyNP15__1down;
   Float_t         weight_EL_EFF_ID_CorrUncertaintyNP15__1up;
   Float_t         weight_EL_EFF_ID_CorrUncertaintyNP1__1down;
   Float_t         weight_EL_EFF_ID_CorrUncertaintyNP1__1up;
   Float_t         weight_EL_EFF_ID_CorrUncertaintyNP2__1down;
   Float_t         weight_EL_EFF_ID_CorrUncertaintyNP2__1up;
   Float_t         weight_EL_EFF_ID_CorrUncertaintyNP3__1down;
   Float_t         weight_EL_EFF_ID_CorrUncertaintyNP3__1up;
   Float_t         weight_EL_EFF_ID_CorrUncertaintyNP4__1down;
   Float_t         weight_EL_EFF_ID_CorrUncertaintyNP4__1up;
   Float_t         weight_EL_EFF_ID_CorrUncertaintyNP5__1down;
   Float_t         weight_EL_EFF_ID_CorrUncertaintyNP5__1up;
   Float_t         weight_EL_EFF_ID_CorrUncertaintyNP6__1down;
   Float_t         weight_EL_EFF_ID_CorrUncertaintyNP6__1up;
   Float_t         weight_EL_EFF_ID_CorrUncertaintyNP7__1down;
   Float_t         weight_EL_EFF_ID_CorrUncertaintyNP7__1up;
   Float_t         weight_EL_EFF_ID_CorrUncertaintyNP8__1down;
   Float_t         weight_EL_EFF_ID_CorrUncertaintyNP8__1up;
   Float_t         weight_EL_EFF_ID_CorrUncertaintyNP9__1down;
   Float_t         weight_EL_EFF_ID_CorrUncertaintyNP9__1up;
   Float_t         weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP0__1down;
   Float_t         weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP0__1up;
   Float_t         weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP10__1down;
   Float_t         weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP10__1up;
   Float_t         weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP11__1down;
   Float_t         weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP11__1up;
   Float_t         weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP12__1down;
   Float_t         weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP12__1up;
   Float_t         weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP13__1down;
   Float_t         weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP13__1up;
   Float_t         weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP14__1down;
   Float_t         weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP14__1up;
   Float_t         weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP15__1down;
   Float_t         weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP15__1up;
   Float_t         weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP16__1down;
   Float_t         weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP16__1up;
   Float_t         weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP17__1down;
   Float_t         weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP17__1up;
   Float_t         weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP1__1down;
   Float_t         weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP1__1up;
   Float_t         weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP2__1down;
   Float_t         weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP2__1up;
   Float_t         weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP3__1down;
   Float_t         weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP3__1up;
   Float_t         weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP4__1down;
   Float_t         weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP4__1up;
   Float_t         weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP5__1down;
   Float_t         weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP5__1up;
   Float_t         weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP6__1down;
   Float_t         weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP6__1up;
   Float_t         weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP7__1down;
   Float_t         weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP7__1up;
   Float_t         weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP8__1down;
   Float_t         weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP8__1up;
   Float_t         weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP9__1down;
   Float_t         weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP9__1up;
   Float_t         weight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down;
   Float_t         weight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up;
   Float_t         weight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down;
   Float_t         weight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up;
   Float_t         weight_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down;
   Float_t         weight_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up;
   Float_t         weight_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down;
   Float_t         weight_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up;
   Float_t         weight_JET_JvtEfficiency__1down;
   Float_t         weight_JET_JvtEfficiency__1up;
   Float_t         weight_MUON_EFF_ISO_STAT__1down;
   Float_t         weight_MUON_EFF_ISO_STAT__1up;
   Float_t         weight_MUON_EFF_ISO_SYS__1down;
   Float_t         weight_MUON_EFF_ISO_SYS__1up;
   Float_t         weight_MUON_EFF_RECO_STAT_LOWPT__1down;
   Float_t         weight_MUON_EFF_RECO_STAT_LOWPT__1up;
   Float_t         weight_MUON_EFF_RECO_STAT__1down;
   Float_t         weight_MUON_EFF_RECO_STAT__1up;
   Float_t         weight_MUON_EFF_RECO_SYS_LOWPT__1down;
   Float_t         weight_MUON_EFF_RECO_SYS_LOWPT__1up;
   Float_t         weight_MUON_EFF_RECO_SYS__1down;
   Float_t         weight_MUON_EFF_RECO_SYS__1up;
   Float_t         weight_MUON_EFF_TTVA_STAT__1down;
   Float_t         weight_MUON_EFF_TTVA_STAT__1up;
   Float_t         weight_MUON_EFF_TTVA_SYS__1down;
   Float_t         weight_MUON_EFF_TTVA_SYS__1up;
   Float_t         weight_MUON_EFF_TrigStatUncertainty__1down;
   Float_t         weight_MUON_EFF_TrigStatUncertainty__1up;
   Float_t         weight_MUON_EFF_TrigSystUncertainty__1down;
   Float_t         weight_MUON_EFF_TrigSystUncertainty__1up;
   Float_t         weight_PRW_DATASF__1down;
   Float_t         weight_PRW_DATASF__1up;
   Float_t         weight_SYSNAMES;
   Float_t         weight_FT_EFF_B_systematics__1down;
   Float_t         weight_FT_EFF_B_systematics__1up;
   Float_t         weight_FT_EFF_C_systematics__1down;
   Float_t         weight_FT_EFF_C_systematics__1up;
   Float_t         weight_FT_EFF_Light_systematics__1down;
   Float_t         weight_FT_EFF_Light_systematics__1up;
   Float_t         weight_FT_EFF_extrapolation__1down;
   Float_t         weight_FT_EFF_extrapolation__1up;
   Float_t         weight_FT_EFF_extrapolation_from_charm__1down;
   Float_t         weight_FT_EFF_extrapolation_from_charm__1up;
   Float_t         weight_var_th_MUR0p5_MUF0p5_PDF261000;
   Float_t         weight_var_th_MUR0p5_MUF1_PDF261000;
   Float_t         weight_var_th_MUR1_MUF0p5_PDF261000;
   Float_t         weight_var_th_MUR1_MUF1_PDF13000;
   Float_t         weight_var_th_MUR1_MUF1_PDF25300;
   Float_t         weight_var_th_MUR1_MUF1_PDF261000;
   Float_t         weight_var_th_MUR1_MUF1_PDF261001;
   Float_t         weight_var_th_MUR1_MUF1_PDF261002;
   Float_t         weight_var_th_MUR1_MUF1_PDF261003;
   Float_t         weight_var_th_MUR1_MUF1_PDF261004;
   Float_t         weight_var_th_MUR1_MUF1_PDF261005;
   Float_t         weight_var_th_MUR1_MUF1_PDF261006;
   Float_t         weight_var_th_MUR1_MUF1_PDF261007;
   Float_t         weight_var_th_MUR1_MUF1_PDF261008;
   Float_t         weight_var_th_MUR1_MUF1_PDF261009;
   Float_t         weight_var_th_MUR1_MUF1_PDF261010;
   Float_t         weight_var_th_MUR1_MUF1_PDF261011;
   Float_t         weight_var_th_MUR1_MUF1_PDF261012;
   Float_t         weight_var_th_MUR1_MUF1_PDF261013;
   Float_t         weight_var_th_MUR1_MUF1_PDF261014;
   Float_t         weight_var_th_MUR1_MUF1_PDF261015;
   Float_t         weight_var_th_MUR1_MUF1_PDF261016;
   Float_t         weight_var_th_MUR1_MUF1_PDF261017;
   Float_t         weight_var_th_MUR1_MUF1_PDF261018;
   Float_t         weight_var_th_MUR1_MUF1_PDF261019;
   Float_t         weight_var_th_MUR1_MUF1_PDF261020;
   Float_t         weight_var_th_MUR1_MUF1_PDF261021;
   Float_t         weight_var_th_MUR1_MUF1_PDF261022;
   Float_t         weight_var_th_MUR1_MUF1_PDF261023;
   Float_t         weight_var_th_MUR1_MUF1_PDF261024;
   Float_t         weight_var_th_MUR1_MUF1_PDF261025;
   Float_t         weight_var_th_MUR1_MUF1_PDF261026;
   Float_t         weight_var_th_MUR1_MUF1_PDF261027;
   Float_t         weight_var_th_MUR1_MUF1_PDF261028;
   Float_t         weight_var_th_MUR1_MUF1_PDF261029;
   Float_t         weight_var_th_MUR1_MUF1_PDF261030;
   Float_t         weight_var_th_MUR1_MUF1_PDF261031;
   Float_t         weight_var_th_MUR1_MUF1_PDF261032;
   Float_t         weight_var_th_MUR1_MUF1_PDF261033;
   Float_t         weight_var_th_MUR1_MUF1_PDF261034;
   Float_t         weight_var_th_MUR1_MUF1_PDF261035;
   Float_t         weight_var_th_MUR1_MUF1_PDF261036;
   Float_t         weight_var_th_MUR1_MUF1_PDF261037;
   Float_t         weight_var_th_MUR1_MUF1_PDF261038;
   Float_t         weight_var_th_MUR1_MUF1_PDF261039;
   Float_t         weight_var_th_MUR1_MUF1_PDF261040;
   Float_t         weight_var_th_MUR1_MUF1_PDF261041;
   Float_t         weight_var_th_MUR1_MUF1_PDF261042;
   Float_t         weight_var_th_MUR1_MUF1_PDF261043;
   Float_t         weight_var_th_MUR1_MUF1_PDF261044;
   Float_t         weight_var_th_MUR1_MUF1_PDF261045;
   Float_t         weight_var_th_MUR1_MUF1_PDF261046;
   Float_t         weight_var_th_MUR1_MUF1_PDF261047;
   Float_t         weight_var_th_MUR1_MUF1_PDF261048;
   Float_t         weight_var_th_MUR1_MUF1_PDF261049;
   Float_t         weight_var_th_MUR1_MUF1_PDF261050;
   Float_t         weight_var_th_MUR1_MUF1_PDF261051;
   Float_t         weight_var_th_MUR1_MUF1_PDF261052;
   Float_t         weight_var_th_MUR1_MUF1_PDF261053;
   Float_t         weight_var_th_MUR1_MUF1_PDF261054;
   Float_t         weight_var_th_MUR1_MUF1_PDF261055;
   Float_t         weight_var_th_MUR1_MUF1_PDF261056;
   Float_t         weight_var_th_MUR1_MUF1_PDF261057;
   Float_t         weight_var_th_MUR1_MUF1_PDF261058;
   Float_t         weight_var_th_MUR1_MUF1_PDF261059;
   Float_t         weight_var_th_MUR1_MUF1_PDF261060;
   Float_t         weight_var_th_MUR1_MUF1_PDF261061;
   Float_t         weight_var_th_MUR1_MUF1_PDF261062;
   Float_t         weight_var_th_MUR1_MUF1_PDF261063;
   Float_t         weight_var_th_MUR1_MUF1_PDF261064;
   Float_t         weight_var_th_MUR1_MUF1_PDF261065;
   Float_t         weight_var_th_MUR1_MUF1_PDF261066;
   Float_t         weight_var_th_MUR1_MUF1_PDF261067;
   Float_t         weight_var_th_MUR1_MUF1_PDF261068;
   Float_t         weight_var_th_MUR1_MUF1_PDF261069;
   Float_t         weight_var_th_MUR1_MUF1_PDF261070;
   Float_t         weight_var_th_MUR1_MUF1_PDF261071;
   Float_t         weight_var_th_MUR1_MUF1_PDF261072;
   Float_t         weight_var_th_MUR1_MUF1_PDF261073;
   Float_t         weight_var_th_MUR1_MUF1_PDF261074;
   Float_t         weight_var_th_MUR1_MUF1_PDF261075;
   Float_t         weight_var_th_MUR1_MUF1_PDF261076;
   Float_t         weight_var_th_MUR1_MUF1_PDF261077;
   Float_t         weight_var_th_MUR1_MUF1_PDF261078;
   Float_t         weight_var_th_MUR1_MUF1_PDF261079;
   Float_t         weight_var_th_MUR1_MUF1_PDF261080;
   Float_t         weight_var_th_MUR1_MUF1_PDF261081;
   Float_t         weight_var_th_MUR1_MUF1_PDF261082;
   Float_t         weight_var_th_MUR1_MUF1_PDF261083;
   Float_t         weight_var_th_MUR1_MUF1_PDF261084;
   Float_t         weight_var_th_MUR1_MUF1_PDF261085;
   Float_t         weight_var_th_MUR1_MUF1_PDF261086;
   Float_t         weight_var_th_MUR1_MUF1_PDF261087;
   Float_t         weight_var_th_MUR1_MUF1_PDF261088;
   Float_t         weight_var_th_MUR1_MUF1_PDF261089;
   Float_t         weight_var_th_MUR1_MUF1_PDF261090;
   Float_t         weight_var_th_MUR1_MUF1_PDF261091;
   Float_t         weight_var_th_MUR1_MUF1_PDF261092;
   Float_t         weight_var_th_MUR1_MUF1_PDF261093;
   Float_t         weight_var_th_MUR1_MUF1_PDF261094;
   Float_t         weight_var_th_MUR1_MUF1_PDF261095;
   Float_t         weight_var_th_MUR1_MUF1_PDF261096;
   Float_t         weight_var_th_MUR1_MUF1_PDF261097;
   Float_t         weight_var_th_MUR1_MUF1_PDF261098;
   Float_t         weight_var_th_MUR1_MUF1_PDF261099;
   Float_t         weight_var_th_MUR1_MUF1_PDF261100;
   Float_t         weight_var_th_MUR1_MUF1_PDF269000;
   Float_t         weight_var_th_MUR1_MUF1_PDF270000;
   Float_t         weight_var_th_MUR1_MUF2_PDF261000;
   Float_t         weight_var_th_MUR2_MUF1_PDF261000;
   Float_t         weight_var_th_MUR2_MUF2_PDF261000;
   Float_t         weight_var_th_pdfset_90400;
   Float_t         weight_var_th_pdfset_90401;
   Float_t         weight_var_th_pdfset_90402;
   Float_t         weight_var_th_pdfset_90403;
   Float_t         weight_var_th_pdfset_90404;
   Float_t         weight_var_th_pdfset_90405;
   Float_t         weight_var_th_pdfset_90406;
   Float_t         weight_var_th_pdfset_90407;
   Float_t         weight_var_th_pdfset_90408;
   Float_t         weight_var_th_pdfset_90409;
   Float_t         weight_var_th_pdfset_90410;
   Float_t         weight_var_th_pdfset_90411;
   Float_t         weight_var_th_pdfset_90412;
   Float_t         weight_var_th_pdfset_90413;
   Float_t         weight_var_th_pdfset_90414;
   Float_t         weight_var_th_pdfset_90415;
   Float_t         weight_var_th_pdfset_90416;
   Float_t         weight_var_th_pdfset_90417;
   Float_t         weight_var_th_pdfset_90418;
   Float_t         weight_var_th_pdfset_90419;
   Float_t         weight_var_th_pdfset_90420;
   Float_t         weight_var_th_pdfset_90421;
   Float_t         weight_var_th_pdfset_90422;
   Float_t         weight_var_th_pdfset_90423;
   Float_t         weight_var_th_pdfset_90424;
   Float_t         weight_var_th_pdfset_90425;
   Float_t         weight_var_th_pdfset_90426;
   Float_t         weight_var_th_pdfset_90427;
   Float_t         weight_var_th_pdfset_90428;
   Float_t         weight_var_th_pdfset_90429;
   Float_t         weight_var_th_pdfset_90430;
   Float_t         weight_var_th_pdfset_90431;
   Float_t         weight_var_th_pdfset_90432;





   map<string,Float_t> SYSName;

void MAPSYS(){
   SYSName = {
   { "weight_EL_EFF_ID_CorrUncertaintyNP0__1down",                         weight_EL_EFF_ID_CorrUncertaintyNP0__1down},                         
   { "weight_EL_EFF_ID_CorrUncertaintyNP0__1up",                           weight_EL_EFF_ID_CorrUncertaintyNP0__1up},                              
   { "weight_EL_EFF_ID_CorrUncertaintyNP10__1down",                        weight_EL_EFF_ID_CorrUncertaintyNP10__1down},
   { "weight_EL_EFF_ID_CorrUncertaintyNP10__1up",                          weight_EL_EFF_ID_CorrUncertaintyNP10__1up},
   { "weight_EL_EFF_ID_CorrUncertaintyNP11__1down",                        weight_EL_EFF_ID_CorrUncertaintyNP11__1down},
   { "weight_EL_EFF_ID_CorrUncertaintyNP11__1up",                          weight_EL_EFF_ID_CorrUncertaintyNP11__1up},
   { "weight_EL_EFF_ID_CorrUncertaintyNP12__1down",                        weight_EL_EFF_ID_CorrUncertaintyNP12__1down},
   { "weight_EL_EFF_ID_CorrUncertaintyNP12__1up",                          weight_EL_EFF_ID_CorrUncertaintyNP12__1up},
   { "weight_EL_EFF_ID_CorrUncertaintyNP13__1down",                        weight_EL_EFF_ID_CorrUncertaintyNP13__1down},
   { "weight_EL_EFF_ID_CorrUncertaintyNP13__1up",                          weight_EL_EFF_ID_CorrUncertaintyNP13__1up},
   { "weight_EL_EFF_ID_CorrUncertaintyNP14__1down",                        weight_EL_EFF_ID_CorrUncertaintyNP14__1down},
   { "weight_EL_EFF_ID_CorrUncertaintyNP14__1up",                          weight_EL_EFF_ID_CorrUncertaintyNP14__1up},
   { "weight_EL_EFF_ID_CorrUncertaintyNP15__1down",                        weight_EL_EFF_ID_CorrUncertaintyNP15__1down},
   { "weight_EL_EFF_ID_CorrUncertaintyNP15__1up",                          weight_EL_EFF_ID_CorrUncertaintyNP15__1up},
   { "weight_EL_EFF_ID_CorrUncertaintyNP1__1down",                         weight_EL_EFF_ID_CorrUncertaintyNP1__1down},
   { "weight_EL_EFF_ID_CorrUncertaintyNP1__1up",                           weight_EL_EFF_ID_CorrUncertaintyNP1__1up},
   { "weight_EL_EFF_ID_CorrUncertaintyNP2__1down",                         weight_EL_EFF_ID_CorrUncertaintyNP2__1down},
   { "weight_EL_EFF_ID_CorrUncertaintyNP2__1up",                           weight_EL_EFF_ID_CorrUncertaintyNP2__1up},
   { "weight_EL_EFF_ID_CorrUncertaintyNP3__1down",                         weight_EL_EFF_ID_CorrUncertaintyNP3__1down},
   { "weight_EL_EFF_ID_CorrUncertaintyNP3__1up",                           weight_EL_EFF_ID_CorrUncertaintyNP3__1up},
   { "weight_EL_EFF_ID_CorrUncertaintyNP4__1down",                         weight_EL_EFF_ID_CorrUncertaintyNP4__1down},
   { "weight_EL_EFF_ID_CorrUncertaintyNP4__1up",                           weight_EL_EFF_ID_CorrUncertaintyNP4__1up},
   { "weight_EL_EFF_ID_CorrUncertaintyNP5__1down",                         weight_EL_EFF_ID_CorrUncertaintyNP5__1down},
   { "weight_EL_EFF_ID_CorrUncertaintyNP5__1up",                           weight_EL_EFF_ID_CorrUncertaintyNP5__1up},
   { "weight_EL_EFF_ID_CorrUncertaintyNP6__1down",                         weight_EL_EFF_ID_CorrUncertaintyNP6__1down},
   { "weight_EL_EFF_ID_CorrUncertaintyNP6__1up",                           weight_EL_EFF_ID_CorrUncertaintyNP6__1up},
   { "weight_EL_EFF_ID_CorrUncertaintyNP7__1down",                         weight_EL_EFF_ID_CorrUncertaintyNP7__1down},
   { "weight_EL_EFF_ID_CorrUncertaintyNP7__1up",                           weight_EL_EFF_ID_CorrUncertaintyNP7__1up},
   { "weight_EL_EFF_ID_CorrUncertaintyNP8__1down",                         weight_EL_EFF_ID_CorrUncertaintyNP8__1down},
   { "weight_EL_EFF_ID_CorrUncertaintyNP8__1up",                           weight_EL_EFF_ID_CorrUncertaintyNP8__1up},
   { "weight_EL_EFF_ID_CorrUncertaintyNP9__1down",                         weight_EL_EFF_ID_CorrUncertaintyNP9__1down},
   { "weight_EL_EFF_ID_CorrUncertaintyNP9__1up",                           weight_EL_EFF_ID_CorrUncertaintyNP9__1up},
   { "weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP0__1down",            weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP0__1down},
   { "weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP0__1up",              weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP0__1up},
   { "weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP10__1down",           weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP10__1down},
   { "weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP10__1up",             weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP10__1up},
   { "weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP11__1down",           weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP11__1down},
   { "weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP11__1up",             weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP11__1up},
   { "weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP12__1down",           weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP12__1down},
   { "weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP12__1up",             weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP12__1up},
   { "weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP13__1down",           weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP13__1down},
   { "weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP13__1up",             weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP13__1up},
   { "weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP14__1down",           weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP14__1down},
   { "weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP14__1up",             weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP14__1up},
   { "weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP15__1down",           weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP15__1down},
   { "weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP15__1up",             weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP15__1up},
   { "weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP16__1down",           weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP16__1down},
   { "weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP16__1up",             weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP16__1up},
   { "weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP17__1down",           weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP17__1down},
   { "weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP17__1up",             weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP17__1up},
   { "weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP1__1down",            weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP1__1down},
   { "weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP1__1up",              weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP1__1up},
   { "weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP2__1down",            weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP2__1down},
   { "weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP2__1up",              weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP2__1up},
   { "weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP3__1down",            weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP3__1down},
   { "weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP3__1up",              weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP3__1up},
   { "weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP4__1down",            weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP4__1down},
   { "weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP4__1up",              weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP4__1up},
   { "weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP5__1down",            weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP5__1down},
   { "weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP5__1up",              weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP5__1up},
   { "weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP6__1down",            weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP6__1down},
   { "weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP6__1up",              weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP6__1up},
   { "weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP7__1down",            weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP7__1down},
   { "weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP7__1up",              weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP7__1up},
   { "weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP8__1down",            weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP8__1down},
   { "weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP8__1up",              weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP8__1up},
   { "weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP9__1down",            weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP9__1down},
   { "weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP9__1up",              weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP9__1up},
   { "weight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down",                   weight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down},
   { "weight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up",                     weight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up},
   { "weight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down",                  weight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down},
   { "weight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up",                    weight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up},
   { "weight_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down",            weight_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down},
   { "weight_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up",              weight_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up},
   { "weight_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down",               weight_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down},
   { "weight_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up",                 weight_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up},
   { "weight_JET_JvtEfficiency__1down",                                    weight_JET_JvtEfficiency__1down},
   { "weight_JET_JvtEfficiency__1up",                                      weight_JET_JvtEfficiency__1up},
   { "weight_MUON_EFF_ISO_STAT__1down",                                    weight_MUON_EFF_ISO_STAT__1down},
   { "weight_MUON_EFF_ISO_STAT__1up",                                      weight_MUON_EFF_ISO_STAT__1up},
   { "weight_MUON_EFF_ISO_SYS__1down",                                     weight_MUON_EFF_ISO_SYS__1down},
   { "weight_MUON_EFF_ISO_SYS__1up",                                       weight_MUON_EFF_ISO_SYS__1up},
   { "weight_MUON_EFF_RECO_STAT_LOWPT__1down",                             weight_MUON_EFF_RECO_STAT_LOWPT__1down},
   { "weight_MUON_EFF_RECO_STAT_LOWPT__1up",                               weight_MUON_EFF_RECO_STAT_LOWPT__1up},
   { "weight_MUON_EFF_RECO_STAT__1down",                                   weight_MUON_EFF_RECO_STAT__1down},
   { "weight_MUON_EFF_RECO_STAT__1up",                                     weight_MUON_EFF_RECO_STAT__1up},
   { "weight_MUON_EFF_RECO_SYS_LOWPT__1down",                              weight_MUON_EFF_RECO_SYS_LOWPT__1down},
   { "weight_MUON_EFF_RECO_SYS_LOWPT__1up",                                weight_MUON_EFF_RECO_SYS_LOWPT__1up},
   { "weight_MUON_EFF_RECO_SYS__1down",                                    weight_MUON_EFF_RECO_SYS__1down},
   { "weight_MUON_EFF_RECO_SYS__1up",                                      weight_MUON_EFF_RECO_SYS__1up},
   { "weight_MUON_EFF_TTVA_STAT__1down",                                   weight_MUON_EFF_TTVA_STAT__1down},
   { "weight_MUON_EFF_TTVA_STAT__1up",                                     weight_MUON_EFF_TTVA_STAT__1up},
   { "weight_MUON_EFF_TTVA_SYS__1down",                                    weight_MUON_EFF_TTVA_SYS__1down},
   { "weight_MUON_EFF_TTVA_SYS__1up",                                      weight_MUON_EFF_TTVA_SYS__1up},
   { "weight_MUON_EFF_TrigStatUncertainty__1down",                         weight_MUON_EFF_TrigStatUncertainty__1down},
   { "weight_MUON_EFF_TrigStatUncertainty__1up",                           weight_MUON_EFF_TrigStatUncertainty__1up},
   { "weight_MUON_EFF_TrigSystUncertainty__1down",                         weight_MUON_EFF_TrigSystUncertainty__1down},
   { "weight_MUON_EFF_TrigSystUncertainty__1up",                           weight_MUON_EFF_TrigSystUncertainty__1up},
   { "weight_PRW_DATASF__1down",                                           weight_PRW_DATASF__1down},
   { "weight_PRW_DATASF__1up",                                             weight_PRW_DATASF__1up},
   {"weight_FT_EFF_B_systematics__1down",                 weight_FT_EFF_B_systematics__1down},          
   {"weight_FT_EFF_B_systematics__1up",                   weight_FT_EFF_B_systematics__1up},
   {"weight_FT_EFF_C_systematics__1down",                 weight_FT_EFF_C_systematics__1down},
   {"weight_FT_EFF_C_systematics__1up",                   weight_FT_EFF_C_systematics__1up},
   {"weight_FT_EFF_Light_systematics__1down",             weight_FT_EFF_Light_systematics__1down},
   {"weight_FT_EFF_Light_systematics__1up",               weight_FT_EFF_Light_systematics__1up},
   {"weight_FT_EFF_extrapolation__1down",                 weight_FT_EFF_extrapolation__1down},
   {"weight_FT_EFF_extrapolation__1up",                   weight_FT_EFF_extrapolation__1up},
   {"weight_FT_EFF_extrapolation_from_charm__1down",      weight_FT_EFF_extrapolation_from_charm__1down},
   {"weight_FT_EFF_extrapolation_from_charm__1up",        weight_FT_EFF_extrapolation_from_charm__1up},
   { "weight_SYSNAMES",                                                    weight_SYSNAMES},
   { "weight_var_th_MUR0p5_MUF0p5_PDF261000",           weight_var_th_MUR0p5_MUF0p5_PDF261000}, 
   { "weight_var_th_MUR0p5_MUF1_PDF261000",           weight_var_th_MUR0p5_MUF1_PDF261000},
   { "weight_var_th_MUR1_MUF0p5_PDF261000",           weight_var_th_MUR1_MUF0p5_PDF261000},
   { "weight_var_th_MUR1_MUF1_PDF13000",            weight_var_th_MUR1_MUF1_PDF13000},
   { "weight_var_th_MUR1_MUF1_PDF25300",            weight_var_th_MUR1_MUF1_PDF25300},
   { "weight_var_th_MUR1_MUF1_PDF261000",           weight_var_th_MUR1_MUF1_PDF261000},
   { "weight_var_th_MUR1_MUF1_PDF261001",           weight_var_th_MUR1_MUF1_PDF261001},
   { "weight_var_th_MUR1_MUF1_PDF261002",           weight_var_th_MUR1_MUF1_PDF261002},
   { "weight_var_th_MUR1_MUF1_PDF261003",           weight_var_th_MUR1_MUF1_PDF261003},
   { "weight_var_th_MUR1_MUF1_PDF261004",           weight_var_th_MUR1_MUF1_PDF261004},
   { "weight_var_th_MUR1_MUF1_PDF261005",           weight_var_th_MUR1_MUF1_PDF261005},
   { "weight_var_th_MUR1_MUF1_PDF261006",           weight_var_th_MUR1_MUF1_PDF261006},
   { "weight_var_th_MUR1_MUF1_PDF261007",           weight_var_th_MUR1_MUF1_PDF261007},
   { "weight_var_th_MUR1_MUF1_PDF261008",           weight_var_th_MUR1_MUF1_PDF261008},
   { "weight_var_th_MUR1_MUF1_PDF261009",           weight_var_th_MUR1_MUF1_PDF261009},
   { "weight_var_th_MUR1_MUF1_PDF261010",           weight_var_th_MUR1_MUF1_PDF261010},
   { "weight_var_th_MUR1_MUF1_PDF261011",           weight_var_th_MUR1_MUF1_PDF261011},
   { "weight_var_th_MUR1_MUF1_PDF261012",           weight_var_th_MUR1_MUF1_PDF261012},
   { "weight_var_th_MUR1_MUF1_PDF261013",           weight_var_th_MUR1_MUF1_PDF261013},
   { "weight_var_th_MUR1_MUF1_PDF261014",           weight_var_th_MUR1_MUF1_PDF261014},
   { "weight_var_th_MUR1_MUF1_PDF261015",           weight_var_th_MUR1_MUF1_PDF261015},
   { "weight_var_th_MUR1_MUF1_PDF261016",           weight_var_th_MUR1_MUF1_PDF261016},
   { "weight_var_th_MUR1_MUF1_PDF261017",           weight_var_th_MUR1_MUF1_PDF261017},
   { "weight_var_th_MUR1_MUF1_PDF261018",           weight_var_th_MUR1_MUF1_PDF261018},
   { "weight_var_th_MUR1_MUF1_PDF261019",           weight_var_th_MUR1_MUF1_PDF261019},
   { "weight_var_th_MUR1_MUF1_PDF261020",           weight_var_th_MUR1_MUF1_PDF261020},
   { "weight_var_th_MUR1_MUF1_PDF261021",           weight_var_th_MUR1_MUF1_PDF261021},
   { "weight_var_th_MUR1_MUF1_PDF261022",           weight_var_th_MUR1_MUF1_PDF261022},
   { "weight_var_th_MUR1_MUF1_PDF261023",           weight_var_th_MUR1_MUF1_PDF261023},
   { "weight_var_th_MUR1_MUF1_PDF261024",           weight_var_th_MUR1_MUF1_PDF261024},
   { "weight_var_th_MUR1_MUF1_PDF261025",           weight_var_th_MUR1_MUF1_PDF261025},
   { "weight_var_th_MUR1_MUF1_PDF261026",           weight_var_th_MUR1_MUF1_PDF261026},
   { "weight_var_th_MUR1_MUF1_PDF261027",           weight_var_th_MUR1_MUF1_PDF261027},
   { "weight_var_th_MUR1_MUF1_PDF261028",           weight_var_th_MUR1_MUF1_PDF261028},
   { "weight_var_th_MUR1_MUF1_PDF261029",           weight_var_th_MUR1_MUF1_PDF261029},
   { "weight_var_th_MUR1_MUF1_PDF261030",           weight_var_th_MUR1_MUF1_PDF261030},
   { "weight_var_th_MUR1_MUF1_PDF261031",           weight_var_th_MUR1_MUF1_PDF261031},
   { "weight_var_th_MUR1_MUF1_PDF261032",           weight_var_th_MUR1_MUF1_PDF261032},
   { "weight_var_th_MUR1_MUF1_PDF261033",           weight_var_th_MUR1_MUF1_PDF261033},
   { "weight_var_th_MUR1_MUF1_PDF261034",           weight_var_th_MUR1_MUF1_PDF261034},
   { "weight_var_th_MUR1_MUF1_PDF261035",           weight_var_th_MUR1_MUF1_PDF261035},
   { "weight_var_th_MUR1_MUF1_PDF261036",           weight_var_th_MUR1_MUF1_PDF261036},
   { "weight_var_th_MUR1_MUF1_PDF261037",           weight_var_th_MUR1_MUF1_PDF261037},
   { "weight_var_th_MUR1_MUF1_PDF261038",           weight_var_th_MUR1_MUF1_PDF261038},
   { "weight_var_th_MUR1_MUF1_PDF261039",           weight_var_th_MUR1_MUF1_PDF261039},
   { "weight_var_th_MUR1_MUF1_PDF261040",           weight_var_th_MUR1_MUF1_PDF261040},
   { "weight_var_th_MUR1_MUF1_PDF261041",           weight_var_th_MUR1_MUF1_PDF261041},
   { "weight_var_th_MUR1_MUF1_PDF261042",           weight_var_th_MUR1_MUF1_PDF261042},
   { "weight_var_th_MUR1_MUF1_PDF261043",           weight_var_th_MUR1_MUF1_PDF261043},
   { "weight_var_th_MUR1_MUF1_PDF261044",           weight_var_th_MUR1_MUF1_PDF261044},
   { "weight_var_th_MUR1_MUF1_PDF261045",           weight_var_th_MUR1_MUF1_PDF261045},
   { "weight_var_th_MUR1_MUF1_PDF261046",           weight_var_th_MUR1_MUF1_PDF261046},
   { "weight_var_th_MUR1_MUF1_PDF261047",           weight_var_th_MUR1_MUF1_PDF261047},
   { "weight_var_th_MUR1_MUF1_PDF261048",           weight_var_th_MUR1_MUF1_PDF261048},
   { "weight_var_th_MUR1_MUF1_PDF261049",           weight_var_th_MUR1_MUF1_PDF261049},
   { "weight_var_th_MUR1_MUF1_PDF261050",           weight_var_th_MUR1_MUF1_PDF261050},
   { "weight_var_th_MUR1_MUF1_PDF261051",           weight_var_th_MUR1_MUF1_PDF261051},
   { "weight_var_th_MUR1_MUF1_PDF261052",           weight_var_th_MUR1_MUF1_PDF261052},
   { "weight_var_th_MUR1_MUF1_PDF261053",           weight_var_th_MUR1_MUF1_PDF261053},
   { "weight_var_th_MUR1_MUF1_PDF261054",           weight_var_th_MUR1_MUF1_PDF261054},
   { "weight_var_th_MUR1_MUF1_PDF261055",           weight_var_th_MUR1_MUF1_PDF261055},
   { "weight_var_th_MUR1_MUF1_PDF261056",           weight_var_th_MUR1_MUF1_PDF261056},
   { "weight_var_th_MUR1_MUF1_PDF261057",           weight_var_th_MUR1_MUF1_PDF261057},
   { "weight_var_th_MUR1_MUF1_PDF261058",           weight_var_th_MUR1_MUF1_PDF261058},
   { "weight_var_th_MUR1_MUF1_PDF261059",           weight_var_th_MUR1_MUF1_PDF261059},
   { "weight_var_th_MUR1_MUF1_PDF261060",           weight_var_th_MUR1_MUF1_PDF261060},
   { "weight_var_th_MUR1_MUF1_PDF261061",           weight_var_th_MUR1_MUF1_PDF261061},
   { "weight_var_th_MUR1_MUF1_PDF261062",           weight_var_th_MUR1_MUF1_PDF261062},
   { "weight_var_th_MUR1_MUF1_PDF261063",           weight_var_th_MUR1_MUF1_PDF261063},
   { "weight_var_th_MUR1_MUF1_PDF261064",           weight_var_th_MUR1_MUF1_PDF261064},
   { "weight_var_th_MUR1_MUF1_PDF261065",           weight_var_th_MUR1_MUF1_PDF261065},
   { "weight_var_th_MUR1_MUF1_PDF261066",           weight_var_th_MUR1_MUF1_PDF261066},
   { "weight_var_th_MUR1_MUF1_PDF261067",           weight_var_th_MUR1_MUF1_PDF261067},
   { "weight_var_th_MUR1_MUF1_PDF261068",           weight_var_th_MUR1_MUF1_PDF261068},
   { "weight_var_th_MUR1_MUF1_PDF261069",           weight_var_th_MUR1_MUF1_PDF261069},
   { "weight_var_th_MUR1_MUF1_PDF261070",           weight_var_th_MUR1_MUF1_PDF261070},
   { "weight_var_th_MUR1_MUF1_PDF261071",           weight_var_th_MUR1_MUF1_PDF261071},
   { "weight_var_th_MUR1_MUF1_PDF261072",           weight_var_th_MUR1_MUF1_PDF261072},
   { "weight_var_th_MUR1_MUF1_PDF261073",           weight_var_th_MUR1_MUF1_PDF261073},
   { "weight_var_th_MUR1_MUF1_PDF261074",           weight_var_th_MUR1_MUF1_PDF261074},
   { "weight_var_th_MUR1_MUF1_PDF261075",           weight_var_th_MUR1_MUF1_PDF261075},
   { "weight_var_th_MUR1_MUF1_PDF261076",           weight_var_th_MUR1_MUF1_PDF261076},
   { "weight_var_th_MUR1_MUF1_PDF261077",           weight_var_th_MUR1_MUF1_PDF261077},
   { "weight_var_th_MUR1_MUF1_PDF261078",           weight_var_th_MUR1_MUF1_PDF261078},
   { "weight_var_th_MUR1_MUF1_PDF261079",           weight_var_th_MUR1_MUF1_PDF261079},
   { "weight_var_th_MUR1_MUF1_PDF261080",           weight_var_th_MUR1_MUF1_PDF261080},
   { "weight_var_th_MUR1_MUF1_PDF261081",           weight_var_th_MUR1_MUF1_PDF261081},
   { "weight_var_th_MUR1_MUF1_PDF261082",           weight_var_th_MUR1_MUF1_PDF261082},
   { "weight_var_th_MUR1_MUF1_PDF261083",           weight_var_th_MUR1_MUF1_PDF261083},
   { "weight_var_th_MUR1_MUF1_PDF261084",           weight_var_th_MUR1_MUF1_PDF261084},
   { "weight_var_th_MUR1_MUF1_PDF261085",           weight_var_th_MUR1_MUF1_PDF261085},
   { "weight_var_th_MUR1_MUF1_PDF261086",           weight_var_th_MUR1_MUF1_PDF261086},
   { "weight_var_th_MUR1_MUF1_PDF261087",           weight_var_th_MUR1_MUF1_PDF261087},
   { "weight_var_th_MUR1_MUF1_PDF261088",           weight_var_th_MUR1_MUF1_PDF261088},
   { "weight_var_th_MUR1_MUF1_PDF261089",           weight_var_th_MUR1_MUF1_PDF261089},
   { "weight_var_th_MUR1_MUF1_PDF261090",           weight_var_th_MUR1_MUF1_PDF261090},
   { "weight_var_th_MUR1_MUF1_PDF261091",           weight_var_th_MUR1_MUF1_PDF261091},
   { "weight_var_th_MUR1_MUF1_PDF261092",           weight_var_th_MUR1_MUF1_PDF261092},
   { "weight_var_th_MUR1_MUF1_PDF261093",           weight_var_th_MUR1_MUF1_PDF261093},
   { "weight_var_th_MUR1_MUF1_PDF261094",           weight_var_th_MUR1_MUF1_PDF261094},
   { "weight_var_th_MUR1_MUF1_PDF261095",           weight_var_th_MUR1_MUF1_PDF261095},
   { "weight_var_th_MUR1_MUF1_PDF261096",           weight_var_th_MUR1_MUF1_PDF261096},
   { "weight_var_th_MUR1_MUF1_PDF261097",           weight_var_th_MUR1_MUF1_PDF261097},
   { "weight_var_th_MUR1_MUF1_PDF261098",           weight_var_th_MUR1_MUF1_PDF261098},
   { "weight_var_th_MUR1_MUF1_PDF261099",           weight_var_th_MUR1_MUF1_PDF261099},
   { "weight_var_th_MUR1_MUF1_PDF261100",           weight_var_th_MUR1_MUF1_PDF261100},
   { "weight_var_th_MUR1_MUF1_PDF269000",           weight_var_th_MUR1_MUF1_PDF269000},
   { "weight_var_th_MUR1_MUF1_PDF270000",           weight_var_th_MUR1_MUF1_PDF270000},
   { "weight_var_th_MUR1_MUF2_PDF261000",           weight_var_th_MUR1_MUF2_PDF261000},
   { "weight_var_th_MUR2_MUF1_PDF261000",           weight_var_th_MUR2_MUF1_PDF261000},
   { "weight_var_th_MUR2_MUF2_PDF261000",           weight_var_th_MUR2_MUF2_PDF261000},
   { "weight_var_th_pdfset_90400",          weight_var_th_pdfset_90400}, 
   { "weight_var_th_pdfset_90401",          weight_var_th_pdfset_90401},
   { "weight_var_th_pdfset_90402",          weight_var_th_pdfset_90402},
   { "weight_var_th_pdfset_90403",          weight_var_th_pdfset_90403},
   { "weight_var_th_pdfset_90404",          weight_var_th_pdfset_90404},
   { "weight_var_th_pdfset_90405",          weight_var_th_pdfset_90405},
   { "weight_var_th_pdfset_90406",          weight_var_th_pdfset_90406},
   { "weight_var_th_pdfset_90407",          weight_var_th_pdfset_90407},
   { "weight_var_th_pdfset_90408",          weight_var_th_pdfset_90408},
   { "weight_var_th_pdfset_90409",          weight_var_th_pdfset_90409},
   { "weight_var_th_pdfset_90410",          weight_var_th_pdfset_90410},
   { "weight_var_th_pdfset_90411",          weight_var_th_pdfset_90411},
   { "weight_var_th_pdfset_90412",          weight_var_th_pdfset_90412},
   { "weight_var_th_pdfset_90413",          weight_var_th_pdfset_90413},
   { "weight_var_th_pdfset_90414",          weight_var_th_pdfset_90414},
   { "weight_var_th_pdfset_90415",          weight_var_th_pdfset_90415},
   { "weight_var_th_pdfset_90416",          weight_var_th_pdfset_90416},
   { "weight_var_th_pdfset_90417",          weight_var_th_pdfset_90417},
   { "weight_var_th_pdfset_90418",          weight_var_th_pdfset_90418},
   { "weight_var_th_pdfset_90419",          weight_var_th_pdfset_90419},
   { "weight_var_th_pdfset_90420",          weight_var_th_pdfset_90420},
   { "weight_var_th_pdfset_90421",          weight_var_th_pdfset_90421},
   { "weight_var_th_pdfset_90422",          weight_var_th_pdfset_90422},
   { "weight_var_th_pdfset_90423",          weight_var_th_pdfset_90423},
   { "weight_var_th_pdfset_90424",          weight_var_th_pdfset_90424},
   { "weight_var_th_pdfset_90425",          weight_var_th_pdfset_90425},
   { "weight_var_th_pdfset_90426",          weight_var_th_pdfset_90426},
   { "weight_var_th_pdfset_90427",          weight_var_th_pdfset_90427},
   { "weight_var_th_pdfset_90428",          weight_var_th_pdfset_90428},
   { "weight_var_th_pdfset_90429",          weight_var_th_pdfset_90429},
   { "weight_var_th_pdfset_90430",          weight_var_th_pdfset_90430},
   { "weight_var_th_pdfset_90431",          weight_var_th_pdfset_90431},
   { "weight_var_th_pdfset_90432",          weight_var_th_pdfset_90432},

   };

}
};
#endif
