#include "tools.h"
#include "NtupleMaker.h"
#include "VBFllvvHM.h"

using namespace std;

int main(int argc, char* argv[] ) {
    string ListName;
    vector<string> InputListList;
    cout<<argv[1]<<endl;
    if(argc ==3) {
        ListName = argv[1];
        VBFllvvHM *MA = new VBFllvvHM(ListName,atoi(argv[2]));
        MA->RunProcessor3lCR();
        delete MA;
    }

    return 0;
}

