
//code for OFFSHELL

#include "getxsec.h"
#include "VBFllvvHM.h"
#include <cstdlib>




double SOW;
double SOWERR;
int FindBinSTXS(double Zpt) {
    int TEST;
    if ((Zpt>0) && (Zpt<75)) TEST =0;
    else if ((Zpt>75) && (Zpt<150)) TEST = 1;
    else if ((Zpt>150) && (Zpt<250)) TEST = 2;
    else if ((Zpt>250) && (Zpt<400)) TEST = 3;
    else if ((Zpt>400)) TEST = 4;
    return TEST;

}



int FindBinEWK(double Zpt) {
    int AA = -1;
    if(Zpt<500)
        AA = (Zpt-25)/5;
    if(Zpt>=500)
        AA =94;

    return AA;
}



VBFllvvHM::VBFllvvHM(string Input,int SampleType):NtupleMaker(Input,SampleType) {
    wrkarea = "/eos/user/j/jugao/VBFllvvHM/";
    doShapeSYS = SampleType;
    doAddCorr = SampleType;
//    bool doVBFSel = 1;
    SYSType = "NOSYS";
//    string Inos;
//    ifstream os;
//    isData = SampleType;
//    os.open(Input.c_str());
//    while(os >> Inos) {
//        InputFileList.push_back(Inos);
//    }
//    if ( Input.length() > 20) {
//        Input.erase(0,33);
//    }
//    filename = 0;
//    Input.erase(Input.length()-5);
//    OutPutName = Input;

}


void VBFllvvHM::ReadTreeSkimed( TTree * sTree ) {
    sTree->SetBranchAddress("run",&run);
    sTree->SetBranchAddress("mcRandomRun",&mcRandomRun);
    sTree->SetBranchAddress("bcid",&bcid);
    sTree->SetBranchAddress("event",&event);
    sTree->SetBranchAddress("PRWHash",&PRWHash);
    sTree->SetBranchAddress("isMC",&isMC);
    sTree->SetBranchAddress("event_type",&event_type);
    sTree->SetBranchAddress("event_3CR",&event_3CR);
    sTree->SetBranchAddress("SR_HM_LM",&SR_HM_LM);
    sTree->SetBranchAddress("weight_pileup",&weight_pileup);
    sTree->SetBranchAddress("weight_gen",&weight_gen);
    sTree->SetBranchAddress("weight_exp",&weight_exp);
    sTree->SetBranchAddress("weight_trig",&weight_trig);
    sTree->SetBranchAddress("weight_jets",&weight_jets);
    sTree->SetBranchAddress("weight_jvt",&weight_jvt);
    sTree->SetBranchAddress("weight",&weight);
    sTree->SetBranchAddress("lepplus_pt",&lepplus_pt);
    sTree->SetBranchAddress("lepminus_pt",&lepminus_pt);
    sTree->SetBranchAddress("lepplus_eta",&lepplus_eta);
    sTree->SetBranchAddress("lepminus_eta",&lepminus_eta);
    sTree->SetBranchAddress("lepplus_phi",&lepplus_phi);
    sTree->SetBranchAddress("lepminus_phi",&lepminus_phi);
    sTree->SetBranchAddress("lepplus_m",&lepplus_m);
    sTree->SetBranchAddress("lepminus_m",&lepminus_m);
    sTree->SetBranchAddress("medium_3rd",&medium_3rd);
    sTree->SetBranchAddress("charge_3rd",&charge_3rd);
    sTree->SetBranchAddress("lep3rd_pt",&lep3rd_pt);
    sTree->SetBranchAddress("lep3rd_eta",&lep3rd_eta);
    sTree->SetBranchAddress("lep3rd_phi",&lep3rd_phi);
    sTree->SetBranchAddress("lep3rd_m",&lep3rd_m);
    sTree->SetBranchAddress("leading_pT_lepton",&leading_pT_lepton);
    sTree->SetBranchAddress("subleading_pT_lepton",&subleading_pT_lepton);
//    sTree->SetBranchAddress("electron_pt",&electron_pt);
//    sTree->SetBranchAddress("muon_pt",&muon_pt);
//    sTree->SetBranchAddress("electron_eta",&electron_eta);
//    sTree->SetBranchAddress("muon_eta",&muon_eta);
    sTree->SetBranchAddress("n_jets",&n_jets);
//    sTree->SetBranchAddress("n_cjets",&n_cjets);
//    sTree->SetBranchAddress("n_fjets",&n_fjets);
//    sTree->SetBranchAddress("pTjet_flead",&pTjet_flead);
//    sTree->SetBranchAddress("n_fjvtjets",&n_fjvtjets);
    sTree->SetBranchAddress("n_bjets",&n_bjets);
    sTree->SetBranchAddress("leading_jet_pt",&leading_jet_pt);
    sTree->SetBranchAddress("leading_jet_eta",&leading_jet_eta);
//    sTree->SetBranchAddress("leading_jet_rapidity",&leading_jet_rapidity);
    sTree->SetBranchAddress("second_jet_pt",&second_jet_pt);
    sTree->SetBranchAddress("second_jet_eta",&second_jet_eta);
//    sTree->SetBranchAddress("second_jet_rapidity",&second_jet_rapidity);
//    sTree->SetBranchAddress("leading_twojets_vsum_pt",&leading_twojets_vsum_pt);
    sTree->SetBranchAddress("jet_sumpt",&jet_sumpt);
//    sTree->SetBranchAddress("jet_vsum_pt",&jet_vsum_pt);
//    sTree->SetBranchAddress("jet_vsum_eta",&jet_vsum_eta);
//    sTree->SetBranchAddress("jet_vsum_phi",&jet_vsum_phi);
//    sTree->SetBranchAddress("jet_vsum_m",&jet_vsum_m);
    sTree->SetBranchAddress("mjj",&mjj);
    sTree->SetBranchAddress("detajj",&detajj);
//    sTree->SetBranchAddress("max_mjj",&max_mjj);
//    sTree->SetBranchAddress("max_detajj",&max_detajj);
//    sTree->SetBranchAddress("cos_Z_2jets",&cos_Z_2jets);
//    sTree->SetBranchAddress("central_Z_2jets",&central_Z_2jets);
//    sTree->SetBranchAddress("jet_central_ST",&jet_central_ST);
//    sTree->SetBranchAddress("jet30_central_ST",&jet30_central_ST);
//    sTree->SetBranchAddress("jet40_central_ST",&jet40_central_ST);
//    sTree->SetBranchAddress("jet50_central_ST",&jet50_central_ST);
//    sTree->SetBranchAddress("n_central_jets",&n_central_jets);
//    sTree->SetBranchAddress("n_central_jets30",&n_central_jets30);
//    sTree->SetBranchAddress("n_central_jets40",&n_central_jets40);
//    sTree->SetBranchAddress("n_central_jets50",&n_central_jets50);
//    sTree->SetBranchAddress("m_leading_jet_pt",&m_leading_jet_pt);
//    sTree->SetBranchAddress("m_leading_jet_eta",&m_leading_jet_eta);
//    sTree->SetBranchAddress("m_leading_jet_rapidity",&m_leading_jet_rapidity);
//    sTree->SetBranchAddress("m_second_jet_pt",&m_second_jet_pt);
//    sTree->SetBranchAddress("m_second_jet_eta",&m_second_jet_eta);
//    sTree->SetBranchAddress("m_second_jet_rapidity",&m_second_jet_rapidity);
//    sTree->SetBranchAddress("m_n_central_jets",&m_n_central_jets);
//    sTree->SetBranchAddress("m_leading_twojets_vsum_pt",&m_leading_twojets_vsum_pt);
//    sTree->SetBranchAddress("dPhiMET_MLeadJet",&dPhiMET_MLeadJet);
//    sTree->SetBranchAddress("dPhiMET_MSecJet",&dPhiMET_MSecJet);
//    sTree->SetBranchAddress("jets_pt",&jets_pt);
//    sTree->SetBranchAddress("jets_eta",&jets_eta);
//    sTree->SetBranchAddress("jets_phi",&jets_phi);
//    sTree->SetBranchAddress("jets_m",&jets_m);
    sTree->SetBranchAddress("met_tst",&met_tst);
    sTree->SetBranchAddress("met_px_tst",&met_px_tst);
    sTree->SetBranchAddress("met_py_tst",&met_py_tst);
//    sTree->SetBranchAddress("met_tst_loose",&met_tst_loose);
//    sTree->SetBranchAddress("met_px_tst_loose",&met_px_tst_loose);
//    sTree->SetBranchAddress("met_py_tst_loose",&met_py_tst_loose);
    sTree->SetBranchAddress("met_soft",&met_soft);
    sTree->SetBranchAddress("met_px_soft",&met_px_soft);
    sTree->SetBranchAddress("met_py_soft",&met_py_soft);
    sTree->SetBranchAddress("llvv_is_WW_ZZ",&llvv_is_WW_ZZ);
//    sTree->SetBranchAddress("met_jets_pt",&met_jets_pt);
//    sTree->SetBranchAddress("met_muon_pt",&met_muon_pt);
//    sTree->SetBranchAddress("met_electron_pt",&met_electron_pt);
//    sTree->SetBranchAddress("dphi_met_jets",&dphi_met_jets);
    sTree->SetBranchAddress("met_signif",&met_signif);
    sTree->SetBranchAddress("mT_ZZ",&mT_ZZ);
    sTree->SetBranchAddress("Z_eta",&Z_eta);
    sTree->SetBranchAddress("Z_pT",&Z_pT);
    sTree->SetBranchAddress("Z_rapidity",&Z_rapidity);
//    sTree->SetBranchAddress("met_obj",&met_obj);
    sTree->SetBranchAddress("mZZ_Truth",&mZZ_Truth);
    sTree->SetBranchAddress("met_Truth",&met_Truth);
    sTree->SetBranchAddress("pT_Z_Truth",&pT_Z_Truth);
    sTree->SetBranchAddress("rho_Truth",&rho_Truth);
    sTree->SetBranchAddress("ZpTomT",&ZpTomT);
    sTree->SetBranchAddress("M2Lep",&M2Lep);
    sTree->SetBranchAddress("dLepR",&dLepR);
    sTree->SetBranchAddress("dMetZPhi",&dMetZPhi);
    sTree->SetBranchAddress("frac_pT",&frac_pT);
    sTree->SetBranchAddress("dPhiJ25met",&dPhiJ25met);
    sTree->SetBranchAddress("dPhiJ100met",&dPhiJ100met);
    sTree->SetBranchAddress("mT_Hinv",&mT_Hinv);
    sTree->SetBranchAddress("ZpTomT_Hinv",&ZpTomT_Hinv);
    sTree->SetBranchAddress("MetOHT",&MetOHT);
    sTree->SetBranchAddress("sumpT_scalar",&sumpT_scalar);

//if do sys running
    if (doShapeSYS) {
        sTree->SetBranchAddress("weight_EL_EFF_ID_CorrUncertaintyNP0__1down", &weight_EL_EFF_ID_CorrUncertaintyNP0__1down );
        sTree->SetBranchAddress("weight_EL_EFF_ID_CorrUncertaintyNP0__1up", &weight_EL_EFF_ID_CorrUncertaintyNP0__1up );
        sTree->SetBranchAddress("weight_EL_EFF_ID_CorrUncertaintyNP10__1down", &weight_EL_EFF_ID_CorrUncertaintyNP10__1down);
        sTree->SetBranchAddress("weight_EL_EFF_ID_CorrUncertaintyNP10__1up", &weight_EL_EFF_ID_CorrUncertaintyNP10__1up);
        sTree->SetBranchAddress("weight_EL_EFF_ID_CorrUncertaintyNP11__1down", &weight_EL_EFF_ID_CorrUncertaintyNP11__1down);
        sTree->SetBranchAddress("weight_EL_EFF_ID_CorrUncertaintyNP11__1up", &weight_EL_EFF_ID_CorrUncertaintyNP11__1up);
        sTree->SetBranchAddress("weight_EL_EFF_ID_CorrUncertaintyNP12__1down", &weight_EL_EFF_ID_CorrUncertaintyNP12__1down);
        sTree->SetBranchAddress("weight_EL_EFF_ID_CorrUncertaintyNP12__1up", &weight_EL_EFF_ID_CorrUncertaintyNP12__1up);
        sTree->SetBranchAddress("weight_EL_EFF_ID_CorrUncertaintyNP13__1down", &weight_EL_EFF_ID_CorrUncertaintyNP13__1down);
        sTree->SetBranchAddress("weight_EL_EFF_ID_CorrUncertaintyNP13__1up", &weight_EL_EFF_ID_CorrUncertaintyNP13__1up);
        sTree->SetBranchAddress("weight_EL_EFF_ID_CorrUncertaintyNP14__1down", &weight_EL_EFF_ID_CorrUncertaintyNP14__1down);
        sTree->SetBranchAddress("weight_EL_EFF_ID_CorrUncertaintyNP14__1up", &weight_EL_EFF_ID_CorrUncertaintyNP14__1up);
        sTree->SetBranchAddress("weight_EL_EFF_ID_CorrUncertaintyNP15__1down", &weight_EL_EFF_ID_CorrUncertaintyNP15__1down);
        sTree->SetBranchAddress("weight_EL_EFF_ID_CorrUncertaintyNP15__1up", &weight_EL_EFF_ID_CorrUncertaintyNP15__1up);
        sTree->SetBranchAddress("weight_EL_EFF_ID_CorrUncertaintyNP1__1down", &weight_EL_EFF_ID_CorrUncertaintyNP1__1down);
        sTree->SetBranchAddress("weight_EL_EFF_ID_CorrUncertaintyNP1__1up", &weight_EL_EFF_ID_CorrUncertaintyNP1__1up);
        sTree->SetBranchAddress("weight_EL_EFF_ID_CorrUncertaintyNP2__1down", &weight_EL_EFF_ID_CorrUncertaintyNP2__1down);
        sTree->SetBranchAddress("weight_EL_EFF_ID_CorrUncertaintyNP2__1up", &weight_EL_EFF_ID_CorrUncertaintyNP2__1up);
        sTree->SetBranchAddress("weight_EL_EFF_ID_CorrUncertaintyNP3__1down", &weight_EL_EFF_ID_CorrUncertaintyNP3__1down);
        sTree->SetBranchAddress("weight_EL_EFF_ID_CorrUncertaintyNP3__1up", &weight_EL_EFF_ID_CorrUncertaintyNP3__1up);
        sTree->SetBranchAddress("weight_EL_EFF_ID_CorrUncertaintyNP4__1down", &weight_EL_EFF_ID_CorrUncertaintyNP4__1down);
        sTree->SetBranchAddress("weight_EL_EFF_ID_CorrUncertaintyNP4__1up", &weight_EL_EFF_ID_CorrUncertaintyNP4__1up);
        sTree->SetBranchAddress("weight_EL_EFF_ID_CorrUncertaintyNP5__1down", &weight_EL_EFF_ID_CorrUncertaintyNP5__1down);
        sTree->SetBranchAddress("weight_EL_EFF_ID_CorrUncertaintyNP5__1up", &weight_EL_EFF_ID_CorrUncertaintyNP5__1up);
        sTree->SetBranchAddress("weight_EL_EFF_ID_CorrUncertaintyNP6__1down", &weight_EL_EFF_ID_CorrUncertaintyNP6__1down);
        sTree->SetBranchAddress("weight_EL_EFF_ID_CorrUncertaintyNP6__1up", &weight_EL_EFF_ID_CorrUncertaintyNP6__1up);
        sTree->SetBranchAddress("weight_EL_EFF_ID_CorrUncertaintyNP7__1down", &weight_EL_EFF_ID_CorrUncertaintyNP7__1down);
        sTree->SetBranchAddress("weight_EL_EFF_ID_CorrUncertaintyNP7__1up", &weight_EL_EFF_ID_CorrUncertaintyNP7__1up);
        sTree->SetBranchAddress("weight_EL_EFF_ID_CorrUncertaintyNP8__1down", &weight_EL_EFF_ID_CorrUncertaintyNP8__1down);
        sTree->SetBranchAddress("weight_EL_EFF_ID_CorrUncertaintyNP8__1up", &weight_EL_EFF_ID_CorrUncertaintyNP8__1up);
        sTree->SetBranchAddress("weight_EL_EFF_ID_CorrUncertaintyNP9__1down", &weight_EL_EFF_ID_CorrUncertaintyNP9__1down);
        sTree->SetBranchAddress("weight_EL_EFF_ID_CorrUncertaintyNP9__1up", &weight_EL_EFF_ID_CorrUncertaintyNP9__1up);
        sTree->SetBranchAddress("weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP0__1down", &weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP0__1down);
        sTree->SetBranchAddress("weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP0__1up", &weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP0__1up);
        sTree->SetBranchAddress("weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP10__1down", &weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP10__1down);
        sTree->SetBranchAddress("weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP10__1up", &weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP10__1up);
        sTree->SetBranchAddress("weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP11__1down", &weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP11__1down);
        sTree->SetBranchAddress("weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP11__1up", &weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP11__1up);
        sTree->SetBranchAddress("weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP12__1down", &weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP12__1down);
        sTree->SetBranchAddress("weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP12__1up", &weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP12__1up);
        sTree->SetBranchAddress("weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP13__1down", &weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP13__1down);
        sTree->SetBranchAddress("weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP13__1up", &weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP13__1up);
        sTree->SetBranchAddress("weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP14__1down", &weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP14__1down);
        sTree->SetBranchAddress("weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP14__1up", &weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP14__1up);
        sTree->SetBranchAddress("weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP15__1down", &weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP15__1down);
        sTree->SetBranchAddress("weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP15__1up", &weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP15__1up);
        sTree->SetBranchAddress("weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP16__1down", &weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP16__1down);
        sTree->SetBranchAddress("weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP16__1up", &weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP16__1up);
        sTree->SetBranchAddress("weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP17__1down", &weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP17__1down);
        sTree->SetBranchAddress("weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP17__1up", &weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP17__1up);
        sTree->SetBranchAddress("weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP1__1down", &weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP1__1down);
        sTree->SetBranchAddress("weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP1__1up", &weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP1__1up);
        sTree->SetBranchAddress("weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP2__1down", &weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP2__1down);
        sTree->SetBranchAddress("weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP2__1up", &weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP2__1up);
        sTree->SetBranchAddress("weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP3__1down", &weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP3__1down);
        sTree->SetBranchAddress("weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP3__1up", &weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP3__1up);
        sTree->SetBranchAddress("weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP4__1down", &weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP4__1down);
        sTree->SetBranchAddress("weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP4__1up", &weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP4__1up);
        sTree->SetBranchAddress("weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP5__1down", &weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP5__1down);
        sTree->SetBranchAddress("weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP5__1up", &weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP5__1up);
        sTree->SetBranchAddress("weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP6__1down", &weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP6__1down);
        sTree->SetBranchAddress("weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP6__1up", &weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP6__1up);
        sTree->SetBranchAddress("weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP7__1down", &weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP7__1down);
        sTree->SetBranchAddress("weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP7__1up", &weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP7__1up);
        sTree->SetBranchAddress("weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP8__1down", &weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP8__1down);
        sTree->SetBranchAddress("weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP8__1up", &weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP8__1up);
        sTree->SetBranchAddress("weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP9__1down", &weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP9__1down);
        sTree->SetBranchAddress("weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP9__1up", &weight_EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP9__1up);
        sTree->SetBranchAddress("weight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down", &weight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down);
        sTree->SetBranchAddress("weight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up", &weight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up);
        sTree->SetBranchAddress("weight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down", &weight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down);
        sTree->SetBranchAddress("weight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up", &weight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up);
        sTree->SetBranchAddress("weight_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down", &weight_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down);
        sTree->SetBranchAddress("weight_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up", &weight_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up);
        sTree->SetBranchAddress("weight_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down", &weight_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down);
        sTree->SetBranchAddress("weight_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up", &weight_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up);
        sTree->SetBranchAddress("weight_JET_JvtEfficiency__1down", &weight_JET_JvtEfficiency__1down);
        sTree->SetBranchAddress("weight_JET_JvtEfficiency__1up", &weight_JET_JvtEfficiency__1up);
        sTree->SetBranchAddress("weight_MUON_EFF_ISO_STAT__1down", &weight_MUON_EFF_ISO_STAT__1down);
        sTree->SetBranchAddress("weight_MUON_EFF_ISO_STAT__1up", &weight_MUON_EFF_ISO_STAT__1up);
        sTree->SetBranchAddress("weight_MUON_EFF_ISO_SYS__1down", &weight_MUON_EFF_ISO_SYS__1down);
        sTree->SetBranchAddress("weight_MUON_EFF_ISO_SYS__1up", &weight_MUON_EFF_ISO_SYS__1up);
        sTree->SetBranchAddress("weight_MUON_EFF_RECO_STAT_LOWPT__1down", &weight_MUON_EFF_RECO_STAT_LOWPT__1down);
        sTree->SetBranchAddress("weight_MUON_EFF_RECO_STAT_LOWPT__1up", &weight_MUON_EFF_RECO_STAT_LOWPT__1up);
        sTree->SetBranchAddress("weight_MUON_EFF_RECO_STAT__1down", &weight_MUON_EFF_RECO_STAT__1down);
        sTree->SetBranchAddress("weight_MUON_EFF_RECO_STAT__1up", &weight_MUON_EFF_RECO_STAT__1up);
        sTree->SetBranchAddress("weight_MUON_EFF_RECO_SYS_LOWPT__1down", &weight_MUON_EFF_RECO_SYS_LOWPT__1down);
        sTree->SetBranchAddress("weight_MUON_EFF_RECO_SYS_LOWPT__1up", &weight_MUON_EFF_RECO_SYS_LOWPT__1up);
        sTree->SetBranchAddress("weight_MUON_EFF_RECO_SYS__1down", &weight_MUON_EFF_RECO_SYS__1down);
        sTree->SetBranchAddress("weight_MUON_EFF_RECO_SYS__1up", &weight_MUON_EFF_RECO_SYS__1up);
        sTree->SetBranchAddress("weight_MUON_EFF_TTVA_STAT__1down", &weight_MUON_EFF_TTVA_STAT__1down);
        sTree->SetBranchAddress("weight_MUON_EFF_TTVA_STAT__1up", &weight_MUON_EFF_TTVA_STAT__1up);
        sTree->SetBranchAddress("weight_MUON_EFF_TTVA_SYS__1down", &weight_MUON_EFF_TTVA_SYS__1down);
        sTree->SetBranchAddress("weight_MUON_EFF_TTVA_SYS__1up", &weight_MUON_EFF_TTVA_SYS__1up);
        sTree->SetBranchAddress("weight_MUON_EFF_TrigStatUncertainty__1down", &weight_MUON_EFF_TrigStatUncertainty__1down);
        sTree->SetBranchAddress("weight_MUON_EFF_TrigStatUncertainty__1up", &weight_MUON_EFF_TrigStatUncertainty__1up);
        sTree->SetBranchAddress("weight_MUON_EFF_TrigSystUncertainty__1down", &weight_MUON_EFF_TrigSystUncertainty__1down);
        sTree->SetBranchAddress("weight_MUON_EFF_TrigSystUncertainty__1up", &weight_MUON_EFF_TrigSystUncertainty__1up);
        sTree->SetBranchAddress("weight_PRW_DATASF__1down", &weight_PRW_DATASF__1down);
        sTree->SetBranchAddress("weight_PRW_DATASF__1up", &weight_PRW_DATASF__1up);
        sTree->SetBranchAddress("weight_FT_EFF_B_systematics__1down",                 &weight_FT_EFF_B_systematics__1down);
        sTree->SetBranchAddress("weight_FT_EFF_B_systematics__1up",                   &weight_FT_EFF_B_systematics__1up);
        sTree->SetBranchAddress("weight_FT_EFF_C_systematics__1down",                 &weight_FT_EFF_C_systematics__1down);
        sTree->SetBranchAddress("weight_FT_EFF_C_systematics__1up",                   &weight_FT_EFF_C_systematics__1up);
        sTree->SetBranchAddress("weight_FT_EFF_Light_systematics__1down",             &weight_FT_EFF_Light_systematics__1down);
        sTree->SetBranchAddress("weight_FT_EFF_Light_systematics__1up",               &weight_FT_EFF_Light_systematics__1up);
        sTree->SetBranchAddress("weight_FT_EFF_extrapolation__1down",                 &weight_FT_EFF_extrapolation__1down);
        sTree->SetBranchAddress("weight_FT_EFF_extrapolation__1up",                   &weight_FT_EFF_extrapolation__1up);
        sTree->SetBranchAddress("weight_FT_EFF_extrapolation_from_charm__1down",      &weight_FT_EFF_extrapolation_from_charm__1down);
        sTree->SetBranchAddress("weight_FT_EFF_extrapolation_from_charm__1up",        &weight_FT_EFF_extrapolation_from_charm__1up);
        sTree->SetBranchAddress("weight_var_th_pdfset_90400",     &weight_var_th_pdfset_90400);
        sTree->SetBranchAddress("weight_var_th_pdfset_90401",     &weight_var_th_pdfset_90401);
        sTree->SetBranchAddress("weight_var_th_pdfset_90402",     &weight_var_th_pdfset_90402);
        sTree->SetBranchAddress("weight_var_th_pdfset_90403",     &weight_var_th_pdfset_90403);
        sTree->SetBranchAddress("weight_var_th_pdfset_90404",     &weight_var_th_pdfset_90404);
        sTree->SetBranchAddress("weight_var_th_pdfset_90405",     &weight_var_th_pdfset_90405);
        sTree->SetBranchAddress("weight_var_th_pdfset_90406",     &weight_var_th_pdfset_90406);
        sTree->SetBranchAddress("weight_var_th_pdfset_90407",     &weight_var_th_pdfset_90407);
        sTree->SetBranchAddress("weight_var_th_pdfset_90408",     &weight_var_th_pdfset_90408);
        sTree->SetBranchAddress("weight_var_th_pdfset_90409",     &weight_var_th_pdfset_90409);
        sTree->SetBranchAddress("weight_var_th_pdfset_90410",     &weight_var_th_pdfset_90410);
        sTree->SetBranchAddress("weight_var_th_pdfset_90411",     &weight_var_th_pdfset_90411);
        sTree->SetBranchAddress("weight_var_th_pdfset_90412",     &weight_var_th_pdfset_90412);
        sTree->SetBranchAddress("weight_var_th_pdfset_90413",     &weight_var_th_pdfset_90413);
        sTree->SetBranchAddress("weight_var_th_pdfset_90414",     &weight_var_th_pdfset_90414);
        sTree->SetBranchAddress("weight_var_th_pdfset_90415",     &weight_var_th_pdfset_90415);
        sTree->SetBranchAddress("weight_var_th_pdfset_90416",     &weight_var_th_pdfset_90416);
        sTree->SetBranchAddress("weight_var_th_pdfset_90417",     &weight_var_th_pdfset_90417);
        sTree->SetBranchAddress("weight_var_th_pdfset_90418",     &weight_var_th_pdfset_90418);
        sTree->SetBranchAddress("weight_var_th_pdfset_90419",     &weight_var_th_pdfset_90419);
        sTree->SetBranchAddress("weight_var_th_pdfset_90420",     &weight_var_th_pdfset_90420);
        sTree->SetBranchAddress("weight_var_th_pdfset_90421",     &weight_var_th_pdfset_90421);
        sTree->SetBranchAddress("weight_var_th_pdfset_90422",     &weight_var_th_pdfset_90422);
        sTree->SetBranchAddress("weight_var_th_pdfset_90423",     &weight_var_th_pdfset_90423);
        sTree->SetBranchAddress("weight_var_th_pdfset_90424",     &weight_var_th_pdfset_90424);
        sTree->SetBranchAddress("weight_var_th_pdfset_90425",     &weight_var_th_pdfset_90425);
        sTree->SetBranchAddress("weight_var_th_pdfset_90426",     &weight_var_th_pdfset_90426);
        sTree->SetBranchAddress("weight_var_th_pdfset_90427",     &weight_var_th_pdfset_90427);
        sTree->SetBranchAddress("weight_var_th_pdfset_90428",     &weight_var_th_pdfset_90428);
        sTree->SetBranchAddress("weight_var_th_pdfset_90429",     &weight_var_th_pdfset_90429);
        sTree->SetBranchAddress("weight_var_th_pdfset_90430",     &weight_var_th_pdfset_90430);
        sTree->SetBranchAddress("weight_var_th_pdfset_90431",     &weight_var_th_pdfset_90431);
        sTree->SetBranchAddress("weight_var_th_pdfset_90432",     &weight_var_th_pdfset_90432);
        sTree->SetBranchAddress("weight_var_th_MUR0p5_MUF0p5_PDF261000",     &weight_var_th_MUR0p5_MUF0p5_PDF261000);
        sTree->SetBranchAddress("weight_var_th_MUR0p5_MUF1_PDF261000",     &weight_var_th_MUR0p5_MUF1_PDF261000);
        sTree->SetBranchAddress("weight_var_th_MUR1_MUF0p5_PDF261000",     &weight_var_th_MUR1_MUF0p5_PDF261000);
        sTree->SetBranchAddress("weight_var_th_MUR1_MUF2_PDF261000",     &weight_var_th_MUR1_MUF2_PDF261000);
        sTree->SetBranchAddress("weight_var_th_MUR2_MUF1_PDF261000",     &weight_var_th_MUR2_MUF1_PDF261000);
        sTree->SetBranchAddress("weight_var_th_MUR2_MUF2_PDF261000",     &weight_var_th_MUR2_MUF2_PDF261000);


    }

}

double VBFllvvHM::GetNormSF(int run,  TFile *InputFile,string FileName) {

    double NormSF;
    int yeard = -1;
    if ( FileName.find("r9364") != string::npos ) {
        lum =36.208;
//        lum =3.219+32.988;
        yeard =16;
    } else if ( FileName.find("r10201") != string::npos ) {
//        lum =44.3074;
        lum =44.307;
        yeard = 17;
    } else if ( FileName.find("r10724") != string::npos ) {
//        lum =59.9372;
        lum =58.450;
        yeard = 18;
    } else {
        cout<<"MCSample Not 16a 16d or 16e"<<endl;
    }

    if (run < 0) {
        cout<<"File not exit"<<endl;
    }
    if ((run > 341367)&& (run < 341386)) {
        NormSF = getxsecVBFHllvv(yeard,run)*lum;
    } else {
        TH1D* hInfo = (TH1D*)InputFile->Get("Hist/hInfo_PFlow");
        double xSec = 0;
        if (run == 345666) {
            xSec = 499.080000 * 1.0000E+00 * 1.0 * 1.5;
        } else if (run ==345723) {
            xSec = 7.1108 * 1.0 * 1.5 * 1.7;
        } else if ((run == 346693)||(run==344306)) {
            xSec =0.07704*1000;
        } else if ((run == 346694)||(run==344476)) {
            xSec=0.01242*1000;
        } else {
//            if (hInfo->GetBinContent(3) == 0) {
            xSec   = hInfo->GetBinContent(1)*2.0/hInfo->GetEntries();
//            } else {
//                xSec   = hInfo->GetBinContent(1)*3.0/hInfo->GetEntries();
//            }


        }
        double SumOfWeight = hInfo->GetBinContent(2);
//        double SOWERR     =hInfo->GetBinError(2);

//BigChange

        NormSF = xSec/SumOfWeight*lum;
        SOW = SumOfWeight;
        SOWERR = hInfo->GetBinContent(3);
//       NormSF = SumOfWeight;
//  cout<<"xSec="<<xSec<<endl;
//  cout<<"SOW="<<SumOfWeight<<endl;
//  cout<<"SOWERR="<<SOWERR<<endl;
    }

//    if ( (run == 345666) || (run == 345723) ) {
//    if ( FileName.find("r9364") != string::npos ) //For SYS only 16a. nomial for 16ade
//    NormSF = NormSF*1.5;
//    }
    return NormSF;

}

double VBFllvvHM::GetNormSF(int run, string FileName,string OutFileName) {
    double NormSF;
    int yeard = -1;
    if ( FileName.find("r9364") != string::npos ) {
//        lum =36.207660;
        lum =3.219+32.988;
        yeard =16;
    } else if ( FileName.find("r10201") != string::npos ) {
//        lum =44.3074;
        lum =44.307;
        yeard = 17;
    } else if ( FileName.find("r10724") != string::npos ) {
//        lum =59.9372;
        lum =58.450;
        yeard = 18;
    } else {
        cout<<"MCSample Not 16a 16d or 16e"<<endl;
    }

//    cout<< yeard<<endl;
//    cout<< OutFileName<<endl;
//    cout<< FileName<<endl;

    if ((run == 364254)||(run ==364285)) {
        if (OutFileName.find("WW")!= string::npos) {
            if ( FileName.find("isZZWW0") != string::npos ) {
                NormSF = getxsecVBFHllvv(yeard,run, 0) * lum ;
            } else if ( FileName.find("isZZWW1") != string::npos ) NormSF = getxsecVBFHllvv(yeard,run, 1) * lum ;
            else if ( FileName.find("isZZWW-1") != string::npos ) NormSF = getxsecVBFHllvv(yeard,run, -1) * lum ;
        } else if (OutPutName.find("ZZ")!= string::npos) {
            NormSF = getxsecVBFHllvv(yeard,run, 2) * lum ;
        }

    } else {
        NormSF = getxsecVBFHllvv(yeard,run)*lum;
    }
    if ( (run == 345666) || (run == 345723) ) NormSF = NormSF*1.5;
    return NormSF;
}

void VBFllvvHM::Loop3lCR(TTree* sTree,string inName ) {
//	cout << inName<<endl;
    for(int ii=0; ii<sTree->GetEntries(); ii++) {
        sTree->GetEntry(ii);
        double weightF;
        string Type = "";
        bool doCrackCut =false;

        if ( (inName.find("r9364") != string::npos) ||(inName.find("data15") != string::npos)||(inName.find("data16") != string::npos  )) {
            doCrackCut = true;
        }

        if(isMC) {
            FileNormSF = GetNormSF(run,inName,OutPutName) *0.83 ;
            if (ii == 0) {
                cout << FileNormSF <<endl;
            }
            weightF = weight * FileNormSF;
        } else weightF =1;
        TLorentzVector MISET(1,1,1,1);
        TLorentzVector lep3rdSET(1,1,1,1);
        MISET.SetPxPyPzE(met_px_tst,met_py_tst,0,sqrt(met_px_tst*met_px_tst+met_py_tst*met_py_tst));
        lep3rdSET.SetPtEtaPhiM(lep3rd_pt,lep3rd_eta,lep3rd_phi,lep3rd_m);
        double MWt =0;
        MCMH3lCRTotal["eee"]->CutFlow2->Fill(0.0,weightF);
        if( !((event_type ==0) || (event_type ==1)) ) {
            continue;
        }
        MCMH3lCRTotal["eee"]->CutFlow2->Fill(1.0,weightF);

        if( event_3CR ==0  ) {
            continue;
        }
        MCMH3lCRTotal["eee"]->CutFlow2->Fill(2.0,weightF);

        if (event_3CR ==1) Type = "mumumu";
        else if (event_3CR ==2) Type = "mumue";
        else if (event_3CR ==3) Type = "eee";
        else if (event_3CR ==4) Type = "eemu";

        if (doCrackCut) {
            if (event_3CR ==2) {
                if( fabs(lep3rd_eta)>1.37 &&  fabs(lep3rd_eta) <1.52) continue;
            } else if (event_3CR ==3) {
                if( (fabs(lep3rd_eta)>1.37 && fabs(lep3rd_eta) <1.52) || (fabs(lepplus_eta)>1.37 && fabs(lepplus_eta) <1.52) || (fabs(lepminus_eta)>1.37 && fabs(lepminus_eta) <1.52) ) {
                    continue;
                }

            } else if (event_3CR ==4) {
                if( (fabs(lepplus_eta)>1.37 && fabs(lepplus_eta) <1.52) || (fabs(lepminus_eta)>1.37 && fabs(lepminus_eta) <1.52) )  {
                    continue;
                }
            }

        }


        MWt = sqrt(2*lep3rdSET.Pt()*MISET.Pt()*(1 - cos(CaldPhi(lep3rdSET.Phi(),MISET.Phi()))));
        MCMH3lCRTotal[Type]->CutFlow1->Fill(0.0,weightF);
        for (auto& tt : MCMH3lCR ) {
            if (tt.first == Type) {
                MCMH3lCRTotal[Type]->CutFlow1->Fill(1.0,weightF);
//		if((medium_3rd == 0) && (lep3rd_pt > 20)) cout << medium_3rd <<" " <<lep3rd_pt<<endl;
//                if((medium_3rd == 1) && (lep3rd_pt < 20)) cout << medium_3rd <<" " <<lep3rd_pt<<endl;
                if( (M2Lep > 106) || (M2Lep < 76) ) cout<<M2Lep<<endl;
                if( (lep3rd_pt) < 20 ) continue;
                MCMH3lCRTotal[Type]->CutFlow1->Fill(2.0,weightF);
                if( (medium_3rd) != 1 ) continue;
                MCMH3lCRTotal[Type]->CutFlow1->Fill(3.0,weightF);
                if( MWt < 60 ) continue;
                MCMH3lCRTotal[Type]->CutFlow1->Fill(4.0,weightF);
                if( n_bjets != 0 ) continue;
                MCMH3lCRTotal[Type]->CutFlow1->Fill(5.0,weightF);
//		if( met_signif < 3 ) continue;

                if (n_jets <2 ) continue;
                MCMH3lCRTotal[Type]->CutFlow1->Fill(6.0,weightF);
                if ((leading_jet_pt < 30)||(second_jet_pt < 30)) continue;
                MCMH3lCRTotal[Type]->CutFlow1->Fill(7.0,weightF);
                MCMH3lCR[Type]->THList[0]->Fill(met_tst,weight);
                MCMH3lCR[Type]->THList[7]->Fill(met_signif,weight);
                MCMH3lCR[Type]->THList[14]->Fill(lep3rd_pt,weight);
                MCMH3lCR[Type]->THList[16]->Fill(lep3rd_phi,weight);
                MCMH3lCR[Type]->THList[15]->Fill(lep3rd_eta,weight);
                MCMH3lCR[Type]->THList[13]->Fill(MWt,weight);
                MCMH3lCR[Type]->THList[4]->Fill(n_bjets,weight);
                MCMH3lCR[Type]->THList[8]->Fill(n_jets,weight);
                MCMH3lCR[Type]->THList[9]->Fill(leading_jet_pt,weight);
                MCMH3lCR[Type]->THList[10]->Fill(second_jet_pt,weight);
                MCMH3lCR[Type]->THList[11]->Fill(mjj,weight);
                MCMH3lCR[Type]->THList[12]->Fill(detajj,weight);
                MCMH3lCR[Type]->THList[17]->Fill(M2Lep,weight);
            }
        }
    }
}

void VBFllvvHM::LoopEMuTau(TTree* sTree, TFile *InputFile, string inName) {
//    using namespace TMVA;
//
//
//    TMVA::Reader *reader = new TMVA::Reader( "!Color:!Silent" );
//    reader->AddVariable( "Z_rapidity", &Z_rapidity );
////    reader->AddVariable( "dPhiJ100met", &dPhiJ100met);
////    reader->AddVariable( "jet_sumpt", &jet_sumpt);
//    reader->AddVariable( "dLepR", &dLepR );
//    reader->AddVariable( "dMetZPhi", &dMetZPhi );
//    reader->AddVariable( "met_signif", &met_signif );
//    reader->AddVariable( "frac_pT",&frac_pT );
//    reader->AddVariable( "MetOHT", &MetOHT );
//    reader->AddVariable( "M2Lep", &M2Lep );
//    reader->AddVariable( "sumpT_scalar", &sumpT_scalar);
////    reader->AddVariable( "n_jets", &n_jets);
////    reader->AddVariable( "leading_jet_pt", &leading_jet_pt);
//    reader->AddSpectator("event_3CR", &event_3CR );
//    reader->AddSpectator("n_bjets", &n_bjets );
//    reader->AddSpectator("event_type", &event_type);
//
//    reader->BookMVA("BDTG method", "/net/ustc_02/users/jugao/LMANAR115/anthem/source/data/TMVAClassification_BDTG_R115_TESTBKS.weights.xml");
////    reader->BookMVA("BDTG method", "/net/ustc_02/users/jugao/LMANA/anthem/source/data/TMVAClassification_BDTG_V2.weights.xml");
////
////    reader->BookMVA("BDTG method", "/net/ustc_02/users/jugao/BDTSCAN/TESTMutiFit/dataset/weights/TMVAClassification_BDTG.weights.xml");



    for(int ii=0; ii<sTree->GetEntries(); ii++) {
        sTree->GetEntry(ii);
        Float_t weightF, weight0;
        if(SYSType == "NOSYS") {
            weight0 = weight;
        } else {
//        cout << SYSType <<endl;
            MAPSYS();
            //theory uncer need to be divide by weight_gen LM
//            if (SYSType.find(("var_th")) != string::npos) {
//                weight0 = SYSName["weight_"+SYSType]/weight_gen*weight;
//            } else {
            weight0 = SYSName["weight_"+SYSType];
//            }
        }
//BTEST
//

        if ( ( (run >= 308092) && (run <= 308092) ) || (run == 345666) || (run == 345706) || (run == 345718) || (run == 345723) || (run == 363356) || (run == 363358) || ( (run >= 364100) && (run <= 364141) ) || ( (run >= 364242) && (run <= 364285) ) ) {
//        if(fabs(weight0) > 10) {
//        weight0 = 1;
            if(fabs(weight_gen) >100) continue;
//        }
        }

        int BX = FindBinEWK(pT_Z_Truth);
        if (doAddCorr > 1) {
            int AX = FindBinSTXS(pT_Z_Truth);
            if (doAddCorr == 2) {
                weight0 = weight*(1+qqC1[AX]);
            } else if (doAddCorr == 3) weight0 = weight*(1+qqC2[AX]);
            else if (doAddCorr == 4) weight0 = weight*(1+qqC3[AX]);
            else if (doAddCorr == 5) weight0 = weight*(1+qqC4[AX]);
            else if (doAddCorr == 6) weight0 = weight*(1+qqC5[AX]);
            else if (doAddCorr == 7) {
                if (n_jets ==0) weight0 = weight*(1+qqC6J0[AX]);
                else if (n_jets ==1) weight0 = weight*(1+qqC6J1[AX]);
                else if (n_jets >=2) weight0 = weight*(1+qqC6J2[AX]);
            } else if (doAddCorr == 8) {
                if (n_jets ==1) weight0 = weight*(1+qqC7J1[AX]);
                else if (n_jets >=2) weight0 = weight*(1+qqC7J2[AX]);
            } else if (doAddCorr == 12) weight0 = weight*(1+ggC1[AX]);
            else if (doAddCorr == 13) weight0 = weight*(1+ggC2[AX]);
            else if (doAddCorr == 14) weight0 = weight*(1+ggC3[AX]);
            else if (doAddCorr == 15) weight0 = weight*(1+ggC4[AX]);
            else if (doAddCorr == 16) weight0 = weight*(1+ggC5[AX]);
            else if (doAddCorr == 17) {
                if (n_jets ==0) weight0 = weight*(1+ggC6J0[AX]);
                else if (n_jets ==1) weight0 = weight*(1+ggC6J1[AX]);
                else if (n_jets >=2) weight0 = weight*(1+ggC6J2[AX]);
            } else if (doAddCorr == 18) {
                if (n_jets ==1) weight0 = weight*(1+ggC7J1[AX]);
                else if (n_jets >=2) weight0 = weight*(1+ggC7J2[AX]);
            } else if (doAddCorr == 20) weight0 = weight*(1+a_zhllbbcorr[BX]);
            else if (doAddCorr == 21) {
                if(a_zhllbbcorr[BX]*a_zhllbbcorr[BX] > 0.01)
                    weight0 = weight*(1+a_zhllbbcorr[BX])*(1+a_zhllbbcorr[BX]*a_zhllbbcorr[BX]);
                else
                    weight0 = weight*(1+a_zhllbbcorr[BX])*(1+0.01);
            } else if (doAddCorr == 22) {
                if(a_zhllbbcorr[BX]*a_zhllbbcorr[BX] > 0.01)
                    weight0 = weight*(1+a_zhllbbcorr[BX])*(1-a_zhllbbcorr[BX]*a_zhllbbcorr[BX]);
                else
                    weight0 = weight*(1+a_zhllbbcorr[BX])*(1-0.01);
            }



        }


        if (run==346693) {
            weight0 = weight0*(1+a_zhllbbcorr[BX]);
            //    if (pT_Z_Truth>25 &&  pT_Z_Truth<50)   {
            //        weight0*=0.0132077;
            //    }
            //    if (pT_Z_Truth>50 &&  pT_Z_Truth<75)   {
            //        weight0*=0.00514318;
            //    }
            //    if (pT_Z_Truth>75 &&  pT_Z_Truth<100)  {
            //        weight0*=(1-0.0050633);
            //    }
            //    if (pT_Z_Truth>100 && pT_Z_Truth<125) {
            //        weight0*=(1-0.0079423);
            //    }
            //    if (pT_Z_Truth>125 && pT_Z_Truth<150) {
            //        weight0*=(1-0.0091692);
            //    }
            //    if (pT_Z_Truth>150 && pT_Z_Truth<175) {
            //        weight0*=(1-0.0114475);
            //    }
            //    if (pT_Z_Truth>175 && pT_Z_Truth<200) {
            //        weight0*=(1-0.0103618);
            //    }
            //    if (pT_Z_Truth>200 && pT_Z_Truth<225) {
            //        weight0*=(1-0.0313323);
            //    }
            //    if (pT_Z_Truth>225 && pT_Z_Truth<250) {
            //        weight0*=(1-0.0392759);
            //    }
            //    if (pT_Z_Truth>250 && pT_Z_Truth<275) {
            //        weight0*=(1-0.0387952);
            //    }
            //    if (pT_Z_Truth>275 && pT_Z_Truth<300) {
            //        weight0*=(1-0.0491737);
            //    }
            //    if (pT_Z_Truth>300 && pT_Z_Truth<325) {
            //        weight0*=(1-0.0827677);
            //    }
            //    if (pT_Z_Truth>325 && pT_Z_Truth<350) {
            //        weight0*=(1-0.064955);
            //    }
            //    if (pT_Z_Truth>350 && pT_Z_Truth<375) {
            //        weight0*=(1-0.114451);
            //    }
            //    if (pT_Z_Truth>375 && pT_Z_Truth<400) {
            //        weight0*=(1-0.111809);
            //    }
            //    if (pT_Z_Truth>400 && pT_Z_Truth<425) {
            //        weight0*=(1-0.095612);
            //    }
            //    if (pT_Z_Truth>425 && pT_Z_Truth<450) {
            //        weight0*=(1-0.160523);
            //    }
            //    if (pT_Z_Truth>450 && pT_Z_Truth<475) {
            //        weight0*=(1-0.11195);
            //    }
            //    if (pT_Z_Truth>475 && pT_Z_Truth<500) {
            //        weight0*=(1-0.149896);
            //    }

        }


        if (run==345666) {
            if (met_Truth>0 && met_Truth<60) {
                weight0*=0.95049841;
            }
            if (met_Truth>60 && met_Truth<70) {
                weight0*=0.94603957;
            }
            if (met_Truth>70 && met_Truth<80) {
                weight0*=0.94116453;
            }
            if (met_Truth>80 && met_Truth<90) {
                weight0*=0.93749643;
            }
            if (met_Truth>90 && met_Truth<100) {
                weight0*=0.93427814;
            }
            if (met_Truth>100 && met_Truth<120) {
                weight0*=0.92784113;
            }
            if (met_Truth>120 && met_Truth<140) {
                weight0*=0.91831967;
            }
            if (met_Truth>140 && met_Truth<160) {
                weight0*=0.90832711;
            }
            if (met_Truth>160 && met_Truth<180) {
                weight0*=0.89836000;
            }
            if (met_Truth>180 && met_Truth<200) {
                weight0*=0.88769693;
            }
            if (met_Truth>200 && met_Truth<250) {
                weight0*=0.87195970;
            }
            if (met_Truth>250 && met_Truth<300) {
                weight0*=0.84713314;
            }
            if (met_Truth>300 && met_Truth<350) {
                weight0*=0.82531651;
            }
            if (met_Truth>350 && met_Truth<400) {
                weight0*=0.80451086;
            }
            if (met_Truth>400 && met_Truth<450) {
                weight0*=0.78480511;
            }
            if (met_Truth>450 && met_Truth<500) {
                weight0*=0.76750962;
            }
            if (met_Truth>500 && met_Truth<600) {
                weight0*=0.74312196;
            }
            if (met_Truth>600 && met_Truth<700) {
                weight0*=0.71224842;
            }
            if (met_Truth>700 && met_Truth<800) {
                weight0*=0.68098102;
            }
            if (met_Truth>800 && met_Truth<5000) {
                weight0*=0.66162730;
            }
        }



        string Type = "";
//        bool OldCut =0;
        doVBFSel = 0;
        if(isMC) {
            FileNormSF = GetNormSF(run,InputFile,inName) ;
            if (ii == 0) {
                cout << FileNormSF <<endl;
                cout << "SOW"<<SOW <<endl;
                cout << "SOWERR"<<SOWERR <<endl;
                VBFP["ee"]->THList[27]->Fill(0.5,SOW);
                VBFP["mumu"]->THList[27]->Fill(0.5,SOW);
                VBFP["ee"]->THList[27]->Fill(1.5,SOWERR);
                VBFP["mumu"]->THList[27]->Fill(1.5,SOWERR);
            }
            weightF = weight0 * FileNormSF;
//Signal no xSec Info
//           weightF = weight0;

        } else {
            weightF =1;
            weight0 = 1;
        }
//        if (event_type > 1 ) continue;

        if (event_type == 0) {
            Type ="mumu";
        } else if(event_type == 1) {
            Type = "ee";
//        } else if(event_type == 2) {
//            Type = "emu";
        }

        VBFPTotal["ee"]->CutFlow1->Fill(0.0,weightF);
        VBFPTotal["mumu"]->CutFlow1->Fill(0.0,weightF);

        bool doCutCheck = 1;

        for (auto& tt : VBFP ) {
            if (tt.first == Type) {
                VBFPTotal[Type]->CutFlow1->Fill(1.0,weightF);

//                float BDTscore=reader->EvaluateMVA("BDTG method");
                if (event_3CR !=0) continue;
                VBFPTotal[Type]->CutFlow1->Fill(2.0,weightF);

                if (!((M2Lep >76)&&(M2Lep <106))) continue;
                VBFPTotal[Type]->CutFlow1->Fill(3.0,weightF);


//                if (met_tst < 120 ) continue;



                //        if (doCutCheck) {
                //        if (n_bjets != 0 ) continue;

                //	if ( (met_tst > 90) && (met_signif > 9)  )
                //        VBFP[Type]->THList[1]->Fill(fabs(dLepR),weight0);

                //	if ( (met_tst > 90) && (fabs(dLepR)<1.8)  )
                //        VBFP[Type]->THList[7]->Fill(met_signif,weight0);

                //	if ( (fabs(dLepR)<1.8)  && (met_signif > 9)  )
                //        VBFP[Type]->THList[0]->Fill(met_tst,weight0);

                //	}

                if (fabs(dMetZPhi) < 2.5 ) continue;
                VBFPTotal[Type]->CutFlow1->Fill(4.0,weightF);


                if (fabs(dLepR)>1.8) continue;
                VBFPTotal[Type]->CutFlow1->Fill(5.0,weightF);

                if (met_tst < 120 ) continue;
                VBFPTotal[Type]->CutFlow1->Fill(6.0,weightF);
//                VBFP[Type]->THList[0]->Fill(met_tst,weight0);
//BIGCHAGNE
                if (met_signif < 10) continue;
                VBFPTotal[Type]->CutFlow1->Fill(7.0,weightF);

                if (fabs(dPhiJ100met) < 0.4 ) continue;
                VBFPTotal[Type]->CutFlow1->Fill(8.0,weightF);

                if (n_bjets != 0 ) continue;
                VBFPTotal[Type]->CutFlow1->Fill(9.0,weightF);

                if ( (n_jets<2)|| ((leading_jet_pt < 30)||(second_jet_pt<30)) || (mjj < 550) || (fabs(detajj) < 4.4) ) {
                    VBFPTotal[Type]->CutFlow1->Fill(9.0,weightF);




//BIGCHAGNE
//            if (BDTscore > 0.4 ) continue;
//            if (BDTscore < 0.2 ) continue;
//                VBFP[Type]->THList[26]->Fill(BDTscore,weight0);
//                VBFP[Type]->THList[29]->Fill(BDTscore,weight0);
                VBFP[Type]->THList[1]->Fill(fabs(dLepR),weight0);
                VBFP[Type]->THList[18]->Fill(mT_ZZ,weight0);
                VBFP[Type]->THList[30]->Fill(Z_rapidity,weight0);
                VBFP[Type]->THList[28]->Fill(Z_pT,weight0);
                VBFP[Type]->THList[0]->Fill(met_tst,weight0);
                VBFP[Type]->THList[6]->Fill(MetOHT,weight0);
                VBFP[Type]->THList[7]->Fill(met_signif,weight0);
                VBFP[Type]->THList[2]->Fill(fabs(dMetZPhi),weight0);
                VBFP[Type]->THList[24]->Fill(Z_eta,weight0);
                VBFP[Type]->THList[25]->Fill(sumpT_scalar,weight0);
                VBFP[Type]->THList[5]->Fill(frac_pT,weight0);
                VBFP[Type]->THList[22]->Fill(met_tst,1);
                VBFP[Type]->THList[17]->Fill(M2Lep,weight0);
                VBFP[Type]->THList[23]->Fill(mT_Hinv,weight0);
                VBFP[Type]->THList[4]->Fill(n_bjets,weight0);
                }



                if (doVBFSel) {
                    if (n_jets <2 ) continue;
                    VBFPTotal[Type]->CutFlow1->Fill(11.0,weightF);

                    if ((leading_jet_pt < 30)||(second_jet_pt<30)) continue;
                    VBFPTotal[Type]->CutFlow1->Fill(12.0,weightF);

//BIGTEST
                    if (mjj < 550) continue;
                    VBFPTotal[Type]->CutFlow1->Fill(13.0,weightF);

                    if (fabs(detajj) < 4.4) continue;
                    VBFPTotal[Type]->CutFlow1->Fill(14.0,weightF);
                }
//BTEST


//                VBFP[Type]->THList2D[0]->Fill(BDTscore,met_tst,weight0);
                VBFP[Type]->THList2D[1]->Fill(dLepR,met_tst,weight0);
//                VBFP[Type]->THList2D[2]->Fill(dLepR,BDTscore,weight0);

//                VBFP[Type]->THList[1]->Fill(dLepR,weight0);
//                VBFP[Type]->THList[3]->Fill(dPhiJ100met,weight0);
                //     VBFP[Type]->THList[5]->Fill(frac_pT,weight0);
                //     VBFP[Type]->THList[6]->Fill(MetOHT,weight0);
                //     VBFP[Type]->THList[7]->Fill(met_signif,weight0);
                //     VBFP[Type]->THList[8]->Fill(n_jets,weight0);
                //     VBFP[Type]->THList[9]->Fill(leading_jet_pt,weight0);
                //     VBFP[Type]->THList[10]->Fill(second_jet_pt,weight0);
                //     VBFP[Type]->THList[11]->Fill(mjj,weight0);
                //     VBFP[Type]->THList[19]->Fill(mjj,1);
                //     VBFP[Type]->THList[12]->Fill(detajj,weight0);
                //     VBFP[Type]->THList[20]->Fill(detajj,1);
//                   cout << weight0 <<endl;
            }
        }
    }
}




void VBFllvvHM::LoopEMuTau(TTree* sTree,string inName ) {

    for(int ii=0; ii<sTree->GetEntries(); ii++) {
        sTree->GetEntry(ii);
        double weightF;
        string Type = "";
        bool OldCut =0;
        doVBFSel = 1;
        if(isMC) {
            FileNormSF = GetNormSF(run,inName,OutPutName) ;
            if (ii == 0)
                cout << FileNormSF <<endl;
            weightF = weight * FileNormSF;
        } else weightF =1;
//        if (event_type > 1 ) continue;

        if (event_type == 0) {
            Type ="mumu";
        } else if(event_type == 1) {
            Type = "ee";
        }

        for (auto& tt : VBFP ) {
            if (tt.first == Type) {
                VBFPTotal[Type]->CutFlow1->Fill(0.0,weightF);
                if (event_3CR !=0) continue;
                VBFPTotal[Type]->CutFlow1->Fill(1.0,weightF);
//                VBFP[Type]->THList[0]->Fill(met_tst,weight);
                if (met_tst < 120 ) continue;
                VBFPTotal[Type]->CutFlow1->Fill(2.0,weightF);

//                VBFP[Type]->THList[1]->Fill(dLepR,weight);
                if (fabs(dLepR)>1.8) continue;
                VBFPTotal[Type]->CutFlow1->Fill(3.0,weightF);

//                VBFP[Type]->THList[2]->Fill(dMetZPhi,weight);
                if (fabs(dMetZPhi) < 2.5 ) continue;
                VBFPTotal[Type]->CutFlow1->Fill(4.0,weightF);

//                VBFP[Type]->THList[3]->Fill(dPhiJ100met,weight);
                if (fabs(dPhiJ100met) < 0.4 ) continue;
                VBFPTotal[Type]->CutFlow1->Fill(5.0,weightF);

//                VBFP[Type]->THList[4]->Fill(n_bjets,weight);
                if (n_bjets != 0 ) continue;
                VBFPTotal[Type]->CutFlow1->Fill(6.0,weightF);

                if(OldCut) {
                    if (frac_pT  > 0.2) continue;
                    VBFPTotal[Type]->CutFlow1->Fill(7.0,weightF);
                    if (MetOHT < 0.4 ) continue;
                    VBFPTotal[Type]->CutFlow1->Fill(8.0,weightF);
                    VBFPTotal[Type]->CutFlow1->Fill(9.0,weightF);
                } else {
//                VBFP[Type]->THList[5]->Fill(frac_pT,weight);
                    if (frac_pT < 0.2) {
                        VBFPTotal[Type]->CutFlow1->Fill(7.0,weightF);

//                    VBFP[Type]->THList[6]->Fill(MetOHT,weight);
                        if (MetOHT >0.4 ) {
                            VBFPTotal[Type]->CutFlow1->Fill(8.0,weightF);
                        }
                    }
//                VBFP[Type]->THList[7]->Fill(met_signif,weight);
                    if (met_signif < 10) continue;
                    VBFPTotal[Type]->CutFlow1->Fill(9.0,weightF);
                }

                if (doVBFSel) {
                    if (n_jets <2 ) continue;
                    VBFPTotal[Type]->CutFlow1->Fill(10.0,weightF);

                    if ((leading_jet_pt < 30)||(second_jet_pt<30)) continue;
                    VBFPTotal[Type]->CutFlow1->Fill(11.0,weightF);

                    if (mjj < 550) continue;
                    VBFPTotal[Type]->CutFlow1->Fill(12.0,weightF);

                    if (fabs(detajj) < 4.4) continue;
                    VBFPTotal[Type]->CutFlow1->Fill(13.0,weightF);

                    VBFP[Type]->THList[0]->Fill(met_tst,weight);
                    VBFP[Type]->THList[1]->Fill(dLepR,weight);
                    VBFP[Type]->THList[2]->Fill(dMetZPhi,weight);
                    VBFP[Type]->THList[3]->Fill(dPhiJ100met,weight);
                    VBFP[Type]->THList[4]->Fill(n_bjets,weight);
                    VBFP[Type]->THList[5]->Fill(frac_pT,weight);
                    VBFP[Type]->THList[6]->Fill(MetOHT,weight);
                    VBFP[Type]->THList[7]->Fill(met_signif,weight);
                    VBFP[Type]->THList[8]->Fill(n_jets,weight);
                    VBFP[Type]->THList[9]->Fill(leading_jet_pt,weight);
                    VBFP[Type]->THList[10]->Fill(second_jet_pt,weight);
                    VBFP[Type]->THList[11]->Fill(mjj,weight);
                    VBFP[Type]->THList[12]->Fill(detajj,weight);
                    VBFP[Type]->THList[18]->Fill(mT_ZZ,weight);
                }
            }
        }
    }
}

void VBFllvvHM::NormPlot( MakeHist* InClass, double NSF) {

    for (auto vec = (InClass->THList).begin(); vec < (InClass->THList).end(); vec++) {
        string TNN = (*vec)->GetName();
        if ((TNN.find("NS")) != string::npos) continue;
        else
            (*vec)->Scale((NSF));
    }
    for (auto vec = (InClass->THList2D).begin(); vec < (InClass->THList2D).end(); vec++) {
        (*vec)->Scale((NSF));
    }


//    InClass->MWT->Scale(NSF);
//    InClass->lep3Pt->Scale(NSF);
//    InClass->lep3Phi->Scale(NSF);
//    InClass->lep3Eta->Scale(NSF);

}

void VBFllvvHM::AddPlot(MakeHist*Base,MakeHist*Indep) {

    for (size_t ii = 0; ii < ((Base->THList).size()); ii++) {
        Base->THList[ii]->Add(Indep->THList[ii]);
    }

    for (size_t ii = 0; ii < ((Base->THList2D).size()); ii++) {
        Base->THList2D[ii]->Add(Indep->THList2D[ii]);
    }

//    Base->MWT->Add(Indep->MWT);
//    Base->lep3Pt->Add(Indep->lep3Pt);
//    Base->lep3Phi->Add(Indep->lep3Phi);
//    Base->lep3Eta->Add(Indep->lep3Eta);


}


void VBFllvvHM::WritePlot(MakeHist*Base, TFile*OutFile) {

    for (auto vec = (Base->THList).begin(); vec < (Base->THList).end(); vec++) {
        (*vec)->Write();
    }
    for (auto vec = (Base->THList2D).begin(); vec < (Base->THList2D).end(); vec++) {
        (*vec)->Write();
    }
//    for (auto vec = (Base->THList2D).begin(); vec < (Base->THList2D).end(); vec++) {
//            (*vec)->Write();
//                } Base->MWT->Write();
//    Base->lep3Pt->Write();
//    Base->lep3Phi->Write();
//    Base->lep3Eta->Write();


}

void VBFllvvHM::WriteCutFlow(MakeHist*Base, TFile*OutFile) {
    Base->CutFlow1->Write();
//    if (doVBFSel)
//        Base->CutFlow2->Write();

}

void VBFllvvHM::RunProcessor3lCR() {
    bool readcutflow = 0;
    TFile* outputfile = new TFile((OutPutName+"Plots.root").c_str(),"recreate");
    typedef std::vector<std::string>::iterator STRING_ITOR;
    STRING_ITOR inF;
    FileNormSF = 0;
//    MakeHist * SumT = new MakeHist("VBF3lCRTAllCh",OutPutName);
//    SumT->MKCutFlow();

    for (auto& x : MCMH3lCRTotal ) {
        x.second = new MakeHist("VBF3lCRT"+x.first,OutPutName);
        x.second->MKTH1D();
        x.second->MKTH2D();
        x.second->MKCutFlow();
    }

    int Itag=0;
    for (inF = InputFileList.begin(); inF != InputFileList.end(); ++inF) {
        Itag++;
        TFile* testF = new TFile((*inF).c_str(),"READ");
        TTree* SkimTree = (TTree*)testF->Get("tree_NOMINAL");

        for (auto& xx : MCMH3lCR ) {
            xx.second = new MakeHist("VBF3lCR"+xx.first+convertDouble(Itag),OutPutName);
            xx.second->MKTH1D();
            xx.second->MKTH2D();
        }

        cout<<"loop file:"<<(*inF).c_str()<<endl;
        ReadTreeSkimed(SkimTree);
        Loop3lCR(SkimTree, (*inF));
        if(isMC) {
            for (auto& yy : MCMH3lCR ) {
//BigChange
                NormPlot(yy.second, FileNormSF);
            }
        }
        for (auto& zz : MCMH3lCR ) {
            AddPlot(MCMH3lCRTotal[zz.first],zz.second );
            delete zz.second;
        }
        delete testF;

    }

    for (auto& zt : MCMH3lCR ) {
        AddPlot(MCMH3lCRTotal["AllCh"],MCMH3lCRTotal[zt.first]);
        MCMH3lCRTotal["AllCh"]->CutFlow1->Add(MCMH3lCRTotal[zt.first]->CutFlow1);
    }
//	SumT->CutFlow1->Add(MCMH3lCRTotal["mumumu"]->CutFlow1);
//	SumT->CutFlow1->Add(MCMH3lCRTotal["mumue"]->CutFlow1);
//	SumT->CutFlow1->Add(MCMH3lCRTotal["eee"]->CutFlow1);
//	SumT->CutFlow1->Add(MCMH3lCRTotal["eemu"]->CutFlow1);

    if(readcutflow) {
//        MCMH3lCRTotal["eee"]->ReadCutFlow(MCMH3lCRTotal["eee"]->CutFlow2);
        MCMH3lCRTotal["mumumu"]->ReadCutFlow(MCMH3lCRTotal["mumumu"]->CutFlow1);
        MCMH3lCRTotal["mumue"]->ReadCutFlow(MCMH3lCRTotal["mumue"]->CutFlow1);
        MCMH3lCRTotal["eee"]->ReadCutFlow(MCMH3lCRTotal["eee"]->CutFlow1);
        MCMH3lCRTotal["eemu"]->ReadCutFlow(MCMH3lCRTotal["eemu"]->CutFlow1);
//        SumT->ReadCutFlow(SumT->CutFlow1);
    }
    outputfile->cd();
    for (auto& xn : MCMH3lCRTotal ) {
        WritePlot(xn.second,outputfile);
        WriteCutFlow(xn.second,outputfile);
    }

//    SumT->CutFlow1->Write();
    outputfile->Close();


}


void VBFllvvHM::RunProcessor() {

    bool readcutflow = 0;
    TFile* outputfile = new TFile((OutPutName+"Plots.root").c_str(),"recreate");
    typedef std::vector<std::string>::iterator STRING_ITOR;
    STRING_ITOR inF;
    FileNormSF = 0;
    for (auto& x : VBFPTotal ) {
        x.second = new MakeHist("VBFHMllvvT"+x.first,OutPutName);
        x.second->MKTH1D();
        x.second->MKTH2D();
        x.second->MKCutFlow();
    }
    int Itag=0;
    for (inF = InputFileList.begin(); inF != InputFileList.end(); ++inF) {
        Itag++;
        TFile* testF = new TFile((*inF).c_str(),"READ");
//        TTree* SkimTree = (TTree*)testF->Get("tree_NOMINAL");
        TTree* SkimTree = (TTree*)testF->Get("tree_PFLOW");

        for (auto& xx : VBFP ) {
            xx.second = new MakeHist("VBFHMllvv"+xx.first+convertDouble(Itag),OutPutName);
            xx.second->MKTH1D();
            xx.second->MKTH2D();
        }
        cout<<"loop file:"<<(*inF).c_str()<<endl;
        ReadTreeSkimed(SkimTree);
//        LoopEMuTau(SkimTree, (*inF));
        LoopEMuTau(SkimTree, testF,(*inF));
        if(isMC) {

            for (auto& yy : VBFP ) {
//BigChange
                NormPlot(yy.second, FileNormSF);
            }

        }

        for (auto& zz : VBFP ) {
            AddPlot(VBFPTotal[zz.first],zz.second );
            delete zz.second;
        }

        delete testF;
    }

    for (auto& zt : VBFPTotal ) {
        AddPlot(VBFPTotal["AllCh"],VBFPTotal[zt.first]);
        VBFPTotal["AllCh"]->CutFlow1->Add(VBFPTotal[zt.first]->CutFlow1);
    }

    if(readcutflow) {
        cout<<"ee events " <<endl;
        VBFPTotal["ee"]->ReadCutFlow(VBFPTotal["ee"]->CutFlow1);
        cout<<" " <<endl;
        cout<<"mumu events " <<endl;
        VBFPTotal["mumu"]->ReadCutFlow(VBFPTotal["mumu"]->CutFlow1);

    }
    outputfile->cd();

    for (auto& xn : VBFPTotal ) {
        WritePlot(xn.second,outputfile);
        WriteCutFlow(xn.second,outputfile);
    }

    outputfile->Close();
}


void VBFllvvHM::RunProcessor(string treename) {

    bool readcutflow = 0;
    TFile* outputfile = new TFile((OutPutName+"Plots.root").c_str(),"recreate");
    typedef std::vector<std::string>::iterator STRING_ITOR;
    STRING_ITOR inF;
    FileNormSF = 0;
    for (auto& x : VBFPTotal ) {
        x.second = new MakeHist("VBFHMllvvT"+x.first,OutPutName);
        x.second->MKTH1D();
        x.second->MKTH2D();
        x.second->MKCutFlow();
    }
    int Itag=0;
    for (inF = InputFileList.begin(); inF != InputFileList.end(); ++inF) {
        Itag++;
        TFile* testF = new TFile((*inF).c_str(),"READ");
        TTree* SkimTree;
        if (doShapeSYS) {
            SkimTree = (TTree*)testF->Get("tree_PFLOW");
            SYSType = treename;
        } else {
            SkimTree = (TTree*)testF->Get(treename.c_str());
        }

        for (auto& xx : VBFP ) {
            xx.second = new MakeHist("VBFHMllvv"+xx.first+convertDouble(Itag),OutPutName);
            xx.second->MKTH1D();
            xx.second->MKTH2D();
        }
        cout<<"loop file:"<<(*inF).c_str()<<endl;
        ReadTreeSkimed(SkimTree);
//        LoopEMuTau(SkimTree, (*inF));
        LoopEMuTau(SkimTree, testF,(*inF));
        if(isMC) {
            for (auto& yy : VBFP ) {
//BigChange
                NormPlot(yy.second, FileNormSF);
            }

        }

        for (auto& zz : VBFP ) {
            AddPlot(VBFPTotal[zz.first],zz.second );
            delete zz.second;
        }

        delete testF;
    }

    for (auto& zt : VBFPTotal ) {
        AddPlot(VBFPTotal["AllCh"],VBFPTotal[zt.first]);
        VBFPTotal["AllCh"]->CutFlow1->Add(VBFPTotal[zt.first]->CutFlow1);
    }

    if(readcutflow) {
        cout<<"ee events " <<endl;
        VBFPTotal["ee"]->ReadCutFlow(VBFPTotal["ee"]->CutFlow1);
        cout<<" " <<endl;
        cout<<"mumu events " <<endl;
        VBFPTotal["mumu"]->ReadCutFlow(VBFPTotal["mumu"]->CutFlow1);

    }
    outputfile->cd();

    for (auto& xn : VBFPTotal ) {
        WritePlot(xn.second,outputfile);
        WriteCutFlow(xn.second,outputfile);
    }

    outputfile->Close();
}
