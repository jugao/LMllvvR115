#include "NtupleMaker.h"

void string_replace( std::string &strBig, const std::string &strsrc, const std::string &strdst) {
    std::string::size_type pos = 0;
    std::string::size_type srclen = strsrc.size();
    std::string::size_type dstlen = strdst.size();

    while( (pos=strBig.find(strsrc, pos)) != std::string::npos ) {
        strBig.replace( pos, srclen, strdst );
        pos += dstlen;
    }
}
std::string GetPathOrURLShortName(std::string strFullName) {
    if (strFullName.empty()) {
        return "";
    }

    string_replace(strFullName, "/", "\\");

    std::string::size_type iPos = strFullName.find_last_of('\\') + 1;

    return strFullName.substr(iPos, strFullName.length() - iPos);
}


double CaldPhi(double phi1, double phi2) {
    double Dphi = 0;
    Dphi = phi1 - phi2;
    if(fabs(Dphi) > Pi) {
        if( Dphi>0 ) {
            Dphi = 2*Pi-Dphi;
        } else {
            Dphi = 2*Pi+Dphi;
        }
    }
    return Dphi;
}


NtupleMaker::NtupleMaker(string Input, int SampleType) {
    wrkarea = "/eos/user/j/jugao/LFVWORK/SkimedRoot/1516/";
    TH1::SetDefaultSumw2();
    string Inos;
    ifstream os;
    isData = SampleType;
    os.open(Input.c_str());
    while(os >> Inos) {
        InputFileList.push_back(Inos);
    }
    if (( Input.length() > 20) || Input.find("/") != string::npos) {
        Input = GetPathOrURLShortName(Input);
    }
    filename = 0;
    Input.erase(Input.length()-5);
    OutPutName = Input;
//    cout<<Input<<endl;
    luminosity = 43813.7*0.001;
    NormSF = 1;
}





void NtupleMaker::ReadTreeLFVO( TChain *chain ) {
    //    TFile *f1 = (TFile *)gROOT->GetListOfFiles()->FindObject((inFileName).c_str());
//    TFile *f1 = new TFile(inFileName.c_str(),"READ");
//    ssTree = (TTree *)f1->Get("LFV");
    chain->SetBranchAddress("EventWeight",&(EventWeight));
    chain->SetBranchAddress("PrwWeight",&(PrwWeight));
    chain->SetBranchAddress("MCWeight",&(MCWeight));
    chain->SetBranchAddress("Event_Trigger_Match",&(Event_Trigger_Match));
    chain->SetBranchAddress("Event_Trigger_Pass",&(Event_Trigger_Pass));
    chain->SetBranchAddress("RunNo",&(RunNo));
    chain->SetBranchAddress("lumB",&(lumB));
    chain->SetBranchAddress("EventTag",&(EventTag));
    chain->SetBranchAddress("eventNumber",&(eventNumber));
    chain->SetBranchAddress("dPhi_ej",&(dPhi_ej));
    chain->SetBranchAddress("dPhi_ll",&(dPhi_ll));
    chain->SetBranchAddress("isKeep",&(isKeepEvent));
    chain->SetBranchAddress("xsec",&(xsec));
    chain->SetBranchAddress("isEvtClean",&(isEvtClean));
    chain->SetBranchAddress("isVertexGood",&(isVertexGood));
    chain->SetBranchAddress("isTrigPass",&(isTrigPass));
    chain->SetBranchAddress("MET_Et",&(MET_Et));
    chain->SetBranchAddress("MET_Phi",&(MET_Phi));
    chain->SetBranchAddress("Event_met", &(event_met));
    chain->SetBranchAddress("Event_met_Eta", &(event_met_eta));
    chain->SetBranchAddress("Event_met_Phi", &(event_met_phi));
    chain->SetBranchAddress("Event_met_M", &(event_met_m));
    chain->SetBranchAddress("Event_mT",&(event_mT));
    chain->SetBranchAddress("Ele_OQ",&(Ele_OQ));
    chain->SetBranchAddress("Ele_Charge",&(Ele_Charge));
    chain->SetBranchAddress("Ele_pt",&(Ele_pt));
    chain->SetBranchAddress("Ele_eta",&(Ele_eta));
    chain->SetBranchAddress("Ele_eta_calo",&(Ele_eta_calo));
    chain->SetBranchAddress("Ele_phi",&(Ele_phi));
    chain->SetBranchAddress("Ele_d0sig",&(Ele_d0sig));
    chain->SetBranchAddress("Ele_dz0",&(Ele_dz0));
    chain->SetBranchAddress("Ele_d0pass",&(Ele_d0pass));
    chain->SetBranchAddress("Ele_z0pass",&(Ele_z0pass));
    chain->SetBranchAddress("Ele_MuonOLR",&(Ele_MuonOLR));
    chain->SetBranchAddress("Ele_ID_Medium",&(Ele_ID_Medium));
    chain->SetBranchAddress("Ele_ID_Tight",&(Ele_ID_Tight));
    chain->SetBranchAddress("Ele_ID_Loose",&(Ele_ID_Loose));
    chain->SetBranchAddress("Ele_Iso",&(Ele_Iso));
    chain->SetBranchAddress("Ele_Iso_Loose",&(Ele_Iso_Loose));
    chain->SetBranchAddress("Ele_isTrigMch",&(Ele_isTrigMch));
    chain->SetBranchAddress("Ele_TrigEff",&(Ele_TrigEff));
    chain->SetBranchAddress("Ele_RecoSF",&(Ele_RecoSF));
    chain->SetBranchAddress("Ele_IsoSF",&(Ele_IsoSF));
    chain->SetBranchAddress("Ele_IDSF",&(Ele_IDSF));
    chain->SetBranchAddress("Ele_L1CaloSF",&(Ele_L1CaloSF));
    chain->SetBranchAddress("Mu_pt",&(Mu_pt));
    chain->SetBranchAddress("Mu_eta",&(Mu_eta));
    chain->SetBranchAddress("Mu_phi",&(Mu_phi));
    chain->SetBranchAddress("Mu_d0sig",&(Mu_d0sig));
    chain->SetBranchAddress("Mu_dz0",&(Mu_dz0));
    chain->SetBranchAddress("Mu_d0pass",&(Mu_d0pass));
    chain->SetBranchAddress("Mu_z0pass",&(Mu_z0pass));
    chain->SetBranchAddress("Mu_charge",&(Mu_charge));
    chain->SetBranchAddress("Mu_isHighPt",&(Mu_isHighPt));
    chain->SetBranchAddress("Mu_isID",&(Mu_isID));
    chain->SetBranchAddress("Mu_Iso",&(Mu_Iso));
    chain->SetBranchAddress("Mu_TrigSF",&(Mu_TrigSF));
    chain->SetBranchAddress("Mu_RecoSF",&(Mu_RecoSF));
    chain->SetBranchAddress("Mu_TrackSF",&(Mu_TrackSF));
    chain->SetBranchAddress("Mu_IsoSF",&(Mu_IsoSF));
    chain->SetBranchAddress("Mu_isTrigMch",&(Mu_isTrigMch));
    chain->SetBranchAddress("Mu_isOverlap",&(Mu_isOverlap));
    chain->SetBranchAddress("Tau_isOverlap",&(Tau_isOverlap));
    chain->SetBranchAddress("Tau_pt",&(Tau_pt));
    chain->SetBranchAddress("Tau_eta",&(Tau_eta));
    chain->SetBranchAddress("Tau_phi",&(Tau_phi));
    chain->SetBranchAddress("Tau_ntracks",&(Tau_ntracks));
    chain->SetBranchAddress("Tau_charge",&(Tau_charge));
    chain->SetBranchAddress("TauSelector",&(TauSelector));
    chain->SetBranchAddress("TauSelector_WithOLR",&(TauSelector_WithOLR));
    chain->SetBranchAddress("Tau_RecoSF",&(Tau_RecoSF));
    chain->SetBranchAddress("Tau_IDSF",&(Tau_IDSF));
    chain->SetBranchAddress("Tau_EleOLRSF",&(Tau_EleOLRSF));
    chain->SetBranchAddress("Jet_pt",&(Jet_pt));
    chain->SetBranchAddress("Jet_eta",&(Jet_eta));
    chain->SetBranchAddress("Jet_phi",&(Jet_phi));
    chain->SetBranchAddress("Jet_m",&(Jet_m));
    chain->SetBranchAddress("Jet_isGoodB",&(Jet_isGoodB));
    chain->SetBranchAddress("Jet_JVTSF",&(Jet_JVTSF));
    chain->SetBranchAddress("Jet_BTInefSF",&(Jet_BTInefSF));
    chain->SetBranchAddress("Jet_BTSF",&(Jet_BTSF));
    chain->SetBranchAddress("N_jets",&(N_jets));
    chain->SetBranchAddress("CleanEventNum",&(m_numCleanEvents ));
    chain->SetBranchAddress("Mu_TriggerMatched",&(Mu_TriggerMatched));
    chain->SetBranchAddress("Ele_TriggerMatched",&(Ele_TriggerMatched));
    chain->SetBranchAddress("Ele_isTight",&(Ele_isTight));
    chain->SetBranchAddress("Ele_isLoose",&(Ele_isLoose));
    chain->SetBranchAddress("Ele_isLoose_IDLoose",&(Ele_isLoose_IDLoose));
}

double NtupleMaker::CalLumNormSF(TChain * chain, double xSec) {
//    TFile *file2 = new TFile(inFileName.c_str(),"READ");
//    TTree *meta = (TTree *)file2->Get("metaTree");
    chain->SetBranchAddress("totalWeightedEntries", &(sumOfWeights));
    chain->SetBranchAddress("inputFileName", &(filename));
    vector<string> fileList;
    double Num=0;
    for(Long64_t i=0; i<chain->GetEntries(); i++) {
        chain->GetEntry(i);
        bool isDuplicated = false;
        for(auto na : fileList) {
            if(*(filename)==na) {
                isDuplicated = true;
                break;
            }
        }
        if(!isDuplicated) {
            fileList.push_back(*(filename));
            Num+=(sumOfWeights);
//            cout<<"bookkeeping "<<(filename)<<endl;
        }
    }
    cout <<luminosity<<endl;
    cout <<xSec<<endl;
    cout <<Num<<endl;
    fileList.clear();
//    delete meta;
//    file2->Close();
    return luminosity*xSec/Num;
}

void NtupleMaker::InitialVarible() {
    ele_pt = 0;
    ele_eta = 0;
    ele_phi = 0;
    isData = 0;
    ele_isTight = 0;
    weight = 0;
    mu_pt = 0;
    mu_eta = 0;
    mu_phi = 0;
    mu_isTight = 0;
    tau_pt = 0;
    tau_eta = 0;
    tau_phi = 0;
    tau_isTight = 0;
    muonTrigSF =1.0;
    muonRecoSF = 1.0;
    muonIsoSF = 1.0;
    muonTtvaSF = 1.0;
    electronTrigSF = 1.0;
    electronIDSF = 1.0;
    electronRecoSF = 1.0;
    electronIsoSF = 1.0;
    electronL1CaloSF = 1.0;
    tauRecoSF = 1.0;
    tauEleOlrSF = 1.0;
    tauEJetIDSF = 1.0;
//    xsecSF = 0;
//    lumSF = 0;
//    NormSF = 0;
    Etag = -1;
    jetJvtSF = 1.0;
//    luminosity = 0;
}


void NtupleMaker::SkimProcessor( string inFileName,TChain *chain ) {
    cout<<(wrkarea+inFileName+"_Skim.root").c_str()<<endl;
    TFile* ntupleF = new TFile((wrkarea+inFileName+"_Skim.root").c_str(),"recreate");
    EMuTauTree = new TTree("Emutau","Emutau");
    EMuTauTree->SetDirectory(ntupleF);
    EMuTauTree->Branch("ele_pt",&ele_pt);
    EMuTauTree->Branch("ele_eta",&ele_eta);
    EMuTauTree->Branch("ele_phi",&ele_phi);
    EMuTauTree->Branch("ele_isTight",&ele_isTight);
    EMuTauTree->Branch("mu_pt",&mu_pt);
    EMuTauTree->Branch("mu_eta",&mu_eta);
    EMuTauTree->Branch("mu_phi",&mu_phi);
    EMuTauTree->Branch("mu_isTight",&mu_isTight);
    EMuTauTree->Branch("tau_pt",&tau_pt);
    EMuTauTree->Branch("tau_eta",&tau_eta);
    EMuTauTree->Branch("tau_phi",&tau_phi);
    EMuTauTree->Branch("tau_isTight",&tau_isTight);
    EMuTauTree->Branch("isData",&isData);
    EMuTauTree->Branch("NormSF",&NormSF);
    EMuTauTree->Branch("weight",&weight);
    EMuTauTree->Branch("Etag",&Etag);
    EMuTauTree->Branch("DPhi_ll",&DPhi_ll);

    cout<<chain->GetEntries()<<endl;
    int cnt0 = 0;
    int cnt1 = 0;
//    int cnt2 = 0;
//    int cnt3 = 0;
//    int cntemu = 0;
//    int cntetau = 0;
//    int cntmutau = 0;
//    int cntemu0 = 0;
//    int cntetau0 = 0;
//    int cntmutau0 = 0;
//    int cntemu1 = 0;
//    int cntetau1 = 0;
//    int cntmutau1 = 0;
    for(int ii=0; ii<chain->GetEntries(); ii++) {
        chain->GetEntry(ii);
        InitialVarible();

//	cout<<"SSTREE"<<endl;
        if(ii == 0) {
            if (isData) {
                CalNMSF = 1;
            } else {
                CalNMSF = CalLumNormSF(meta,xsec);
                cout << CalNMSF <<endl;
            }
        }

        if( Ele_TriggerMatched == false && Mu_TriggerMatched == false ) continue;
//	cnt1++;
        if( EventTag > 2 ) continue;
//	cnt2++;
//	if (fabs(dPhi_ll) > 2.7) {
//	if( EventTag == 0) cntemu0++;
//	if( EventTag == 1) cntetau0++;
//	if( EventTag == 2) cntmutau0++;
//	}
//
//	if ((fabs(dPhi_ll) > 2.7)&& Jet_isGoodB) {
//	if( EventTag == 0) cntemu1++;
//	if( EventTag == 1) cntetau1++;
//	if( EventTag == 2) cntmutau1++;
//	}

        cnt0++;
//	cout <<dPhi_ll<<endl;
        if(fabs(dPhi_ll) < 2.7) continue;
//	cnt3++;

//	if(
//        if( MET_Et > 30 ) continue;
        weight = EventWeight;
        Etag = EventTag;
        xsecSF = xsec;
        NormSF = CalNMSF;


//            weight = EventWeight;

//        bool GBJ = 0;
//        if(EventTag == 0) {
//	for(size_t pp= 0; pp<(Jet_isGoodB)->size();pp++){
//		if ((*Jet_isGoodB)[pp]) GBJ =1;
//	}
//	if (GBJ ==1) continue;
//	}
//	cnt1++;

        for (size_t jj=0; jj<(Ele_ID_Medium)->size(); jj++) {
//            if (EventTag == 0) {
//                if (!(((*Ele_ID_Medium)[jj] == 1)||((((*Ele_ID_Tight)[jj] == 1)&&((*Ele_Iso)[jj] == 1))))) continue;
//                if (!(((*Ele_ID_Medium)[jj] == 1))) continue;
//            }
//            if ((EventTag == 1)||(EventTag == 2)) {
            if (!((((*Ele_ID_Tight)[jj] == 1)&&((*Ele_Iso)[jj] == 1)))) continue;
//            }

            ele_pt = (*Ele_pt)[jj];
            ele_eta = (*Ele_eta)[jj];
            ele_phi = (*Ele_phi)[jj];
            ele_isTight = (*Ele_isTight)[jj];
            electronL1CaloSF = (*Ele_L1CaloSF)[jj];
            electronIDSF = (*Ele_IDSF)[jj];
            electronIsoSF = (*Ele_IsoSF)[jj];
            electronRecoSF = (*Ele_RecoSF)[jj];
            electronTrigSF = (*Ele_TrigEff)[jj];
        }

        for (size_t kk=0; kk<(Mu_pt)->size(); kk++) {
            if(!((*Mu_d0pass)[kk])) continue;
            if(!((*Mu_z0pass)[kk])) continue;
            if(((*Mu_isOverlap)[kk])) continue;
            if ((EventTag ==0)||(EventTag == 2)) {
                if(!(*Mu_isHighPt)[kk]) continue;
//		if (EventTag == 2){
                if(!((*Mu_Iso)[kk])) continue;
            }
            if(EventTag ==1) {
                if(!(*Mu_isHighPt)[kk]) continue;
                if(!((*Mu_Iso)[kk])) continue;
            }

            mu_pt = (*Mu_pt)[kk];
            mu_eta = (*Mu_eta)[kk];
            mu_phi = (*Mu_phi)[kk];
            mu_isTight = (*Mu_isHighPt)[kk];
            muonRecoSF = ((*Mu_RecoSF)[kk]);
            muonIsoSF = ((*Mu_IsoSF)[kk]);
            muonTtvaSF = ((*Mu_TrackSF)[kk]);
            muonTrigSF = ((*Mu_TrigSF)[kk]);
        }

        for (size_t ll=0; ll<(Tau_pt)->size(); ll++) {
            if (!(*TauSelector)[ll]) continue;
            if (!(*TauSelector_WithOLR)[ll]) continue;
            if ((*Tau_isOverlap)[ll]) continue;

            tau_pt = (*Tau_pt)[ll];
            tau_eta = (*Tau_eta)[ll];
            tau_phi = (*Tau_phi)[ll];
            tau_isTight = (*TauSelector_WithOLR)[ll];
            tauRecoSF =(*Tau_RecoSF)[ll];
            tauEJetIDSF = (*Tau_IDSF)[ll];
            tauEleOlrSF = (*Tau_EleOLRSF)[ll];
        }

        for (size_t mm= 0; mm<(Jet_JVTSF)->size(); mm++) {
            jetJvtSF *= (*Jet_JVTSF)[mm];
        }
        if (isData) {
            weight = 1;
        } else {
            if( EventTag == 0) weight = PrwWeight*MCWeight* muonTrigSF*muonRecoSF*muonIsoSF*muonTtvaSF*electronTrigSF*electronIDSF*electronRecoSF*electronIsoSF*electronL1CaloSF*jetJvtSF;
            if( EventTag == 1) weight = PrwWeight*MCWeight*electronTrigSF*electronIDSF*electronRecoSF*electronIsoSF*electronL1CaloSF*tauRecoSF*tauEleOlrSF*tauEJetIDSF*jetJvtSF;
            if( EventTag == 2) weight = PrwWeight*MCWeight*muonTrigSF*muonRecoSF*muonIsoSF*muonTtvaSF*tauRecoSF*tauEleOlrSF*tauEJetIDSF*jetJvtSF;
        }




        if(Etag == 0) {
            DPhi_ll = CaldPhi(ele_phi, mu_phi);
        }
        if(Etag == 1) {
            DPhi_ll = CaldPhi(ele_phi, tau_phi);
        }
        if(Etag == 2) {
            DPhi_ll = CaldPhi(mu_phi, tau_phi);
        }
//        cout << DPhi_ll <<endl;

        EMuTauTree->Fill();
    }

    cout<<"     "<<endl;
    cout << cnt0 << endl;
    cout << cnt1 << endl;
//    cout << cnt2 << endl;
//    cout << cnt3 << endl;
//    cout << cntemu << endl;
//    cout << cntetau << endl;
//    cout << cntmutau << endl;
//
//    cout << cntemu0 << endl;
//    cout << cntetau0 << endl;
//    cout << cntmutau0 << endl;
//    cout << cntemu1 << endl;
//    cout << cntetau1 << endl;
//    cout << cntmutau1 << endl;

    ntupleF->cd();
    EMuTauTree->Write();
    ntupleF->Close();
//   delete ntupleF;
//   delete EMuTauTree;
}


void NtupleMaker::RunSkim() {
    TChain *ssTree = new TChain("LFV");
    meta = new TChain("metaTree");
    typedef std::vector<std::string>::iterator STRING_ITOR;
    STRING_ITOR inF;
    for (inF = InputFileList.begin(); inF != InputFileList.end(); ++inF) {
        cout<<*inF<<endl;
        ssTree->Add((*inF).c_str());
        meta->Add((*inF).c_str());
    }
    cout<<OutPutName<<endl;
    ReadTreeLFVO(ssTree);

    SkimProcessor(OutPutName,ssTree);
    delete ssTree;
    delete meta;

}
